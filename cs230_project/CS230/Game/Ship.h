/*--------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name: Ship.h
Purpose: Header file for Ship
Project: CS230
Author: Haewon Shon
Creation date: 4/1/2020
-----------------------------------------------------------------*/
#pragma once

#include "../Engine/BasicDataTypes.h" // Vector2D
#include "../Engine/Sprite.h"
#include "../Engine/Input.h" 
#include "../Engine/GameObject.h"

enum class GameObjectType;

class Ship : public GameObject 
{
public:
	Ship(Vector2D startPos);
	void Update(double dt) override;

	void Draw(TransformMatrix cameraMatrix) override;

	GameObjectType GetObjectType() override;
	std::string GetObjectTypeName() override;
	bool CanCollideWith(GameObjectType collideAgainstType) override;
	void ResolveCollision(GameObject* collidedWith) override;

	bool IsDead();
private:

	static const double acceleration;
	static const double maxVelocity;
	static const double drag;
	static const double rotationRate;

	bool isAccelerating;

	Sprite flame1;
	Sprite flame2;

	Input::InputKey rotateCounterKey;
	Input::InputKey rotateClockKey;
	Input::InputKey accelerateKey;
	Input::InputKey fireLazerKey;

	bool isDead;
};
