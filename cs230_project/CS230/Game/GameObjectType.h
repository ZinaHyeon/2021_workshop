/*--------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name: GameObjectType.h
Purpose: Header file for Game Object Types
Project: CS230
Author: Kevin Wright
Creation date: 5/22/2020
-----------------------------------------------------------------*/

#pragma once

enum class GameObjectType {
    Hero,
    Bunny,
    DeadBunny,
    Ball,
    TreeStump,
    Floor,
    Exit,
    Meteor,
    Ship,
    Laser,
    Particle,
    EnemyShip,
    Count,
};
