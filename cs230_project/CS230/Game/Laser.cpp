/*--------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name: Laser.cpp
Purpose: Laser source file
Project: CS230
Author: Kevin Wright
Creation date: 6/8/2020
-----------------------------------------------------------------*/

#include "Laser.h"
#include"GameObjectType.h"
#include "../Engine/Engine.h" // Engine::GetWindow().GetSize

Laser::Laser(Vector2D pos, double rotation, Vector2D scale, Vector2D laserVelocity)
	: GameObject(pos, rotation, scale)
{
	velocity = laserVelocity;

	sprite.Load("assets/laser.spt", this);
}

void Laser::Update(double dt)
{
	GameObject::Update(dt);
	if (IsOutOfScreen() == true)
	{
		GameObject::Destroy();
	}
}

GameObjectType Laser::GetObjectType()
{
	return GameObjectType::Laser;
}

bool Laser::IsOutOfScreen()
{
	Vector2D windowSize = Engine::GetWindow().GetSize();
	Vector2D lazerSize = sprite.GetFrameSize();

	if ((GetPosition().x > windowSize.x + lazerSize.x / 2.0)
		|| (GetPosition().x < -lazerSize.x / 2.0)
		|| (GetPosition().y > windowSize.y + lazerSize.y / 2.0)
		|| (GetPosition().y < -lazerSize.y / 2.0))
	{
		return true;
	}
	else
	{
		return false;
	}
}