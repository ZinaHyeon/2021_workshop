/*--------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name: EnemyShip.cpp
Purpose: Source file for Ship
Project: CS230
Author: Haewon Shon
Creation date: 6/26/2020
-----------------------------------------------------------------*/

#include "EnemyShip.h"
#include "Flame_Anims.h"
#include "Ship_Anims.h"
#include "../Engine/Engine.h" // Engine::GetLogger
#include "../Engine/TransformMatrix.h" // TransformMatrix
#include "../Engine/Collision.h" // collision
#include "GameObjectType.h" // GameObjectType::Ship
#include "Laser.h"
#include "ScreenWrap.h"
#include "../Engine/TransformMatrix.h" // rotation matrix

constexpr double PI = 3.1415926535897932384626433832795;

const double EnemyShip::acceleration = 270.0;
const double EnemyShip::maxVelocity = 450.0;
const double EnemyShip::drag = 0.9;
const double EnemyShip::rotationRate = 2.0;

EnemyShip::EnemyShip(GameObject* target)
	: GameObject({ static_cast<double>(rand() % static_cast<int> (Engine::GetWindow().GetSize().x)), static_cast<double>(rand() % static_cast<int> (Engine::GetWindow().GetSize().y)) },
	((rand() % static_cast<int>(PI * 2 * 1024)) / 1024.0f), { 0.75, 0.75 }), target(target)
{
	isAccelerating = false;
	sprite.Load("assets/EnemyShip.spt", this);
	sprite.PlayAnimation(static_cast<int>(Flame_Anim::None_Anim));

	flame1.Load("assets/flame.spt", this);
	flame2.Load("assets/flame.spt", this);
	flame1.PlayAnimation(static_cast<int>(Flame_Anim::Flame_Anim));
	flame2.PlayAnimation(static_cast<int>(Flame_Anim::Flame_Anim));

	isDead = false;

	components.AddComponent(new ScreenWrap(this));
}

void EnemyShip::Update(double dt)
{
	Vector2D faceVector = RotateMatrix(GetRotation()) * Vector2D { 0, 1 };
	Vector2D thisToTarget = (target->GetPosition() - GetPosition()).Normalize();

	double crossResult = 0; //Cross(faceVector, thisToTarget);
	if (crossResult > 0.05)
	{
		UpdateRotation(rotationRate * dt);
	}
	else if(crossResult < -0.05)
	{
		UpdateRotation(-rotationRate * dt);
	}

	velocity += RotateMatrix(GetRotation()) * Vector2D { 0, acceleration * dt };

	double trueVelocity = std::sqrt(velocity.LengthSquared());
	if (trueVelocity > maxVelocity)
	{
		velocity = velocity.Normalize() * maxVelocity;
	}

	velocity -= (velocity * EnemyShip::drag * dt);
	UpdatePosition(velocity * dt);

	sprite.Update(dt);
	flame1.Update(dt);
	flame2.Update(dt);

	components.GetComponent<ScreenWrap>()->Wrap();
}

void EnemyShip::Draw(TransformMatrix cameraMatrix)
{
	TransformMatrix displayMatrix = cameraMatrix * GetMatrix();
	sprite.Draw(displayMatrix);
	flame1.Draw(displayMatrix * TranslateMatrix(sprite.GetHotSpot(1)));
	flame2.Draw(displayMatrix * TranslateMatrix(sprite.GetHotSpot(2)));

	if (Engine::Instance().ShowCollision() == true && GetComponent<Collision>() != nullptr)
	{
		GetComponent<Collision>()->Draw(displayMatrix);
	}
}

GameObjectType EnemyShip::GetObjectType()
{
	return GameObjectType::EnemyShip;
}

std::string EnemyShip::GetObjectTypeName()
{
	return "EnemyShip";
}

bool EnemyShip::CanCollideWith(GameObjectType collideAgainstType)
{
	if (collideAgainstType == GameObjectType::Particle)
	{
		return false;
	}
	else
	{
		return true;
	}
}

void EnemyShip::ResolveCollision(GameObject* collidedWith)
{
	if (isDead == true)
	{
		return;
	}

	if (collidedWith->GetObjectType() == GameObjectType::Laser)
	{
		sprite.PlayAnimation(static_cast<int>(Ship_Anim::Explosion_Anim));
		flame1.PlayAnimation(static_cast<int>(Flame_Anim::None_Anim));
		flame2.PlayAnimation(static_cast<int>(Flame_Anim::None_Anim));
		isDead = true;
		collidedWith->Destroy();
	}
}

bool EnemyShip::IsDead()
{
	return isDead;
}