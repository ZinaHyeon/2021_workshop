/*--------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name: Hero.cpp
Purpose: Source file for Hero
Project: CS230
Author: Haewon Shon
Creation date: 4/1/2020
-----------------------------------------------------------------*/

#include "Ball.h"
#include "Level1.h" // Level1::gravity
#include "Ball_Anims.h"
#include "../Engine/Engine.h" // Debug ball velocity
#include "GameObjectType.h" // GameObjectType::Ball;
#include "../Engine/Collision.h"
#include "Gravity.h"

const double Ball::bounceVelocity = 650.0;

Ball::Ball(Vector2D startPos) : GameObject(startPos) {
	sprite.Load("assets/Ball.spt", this);
	currState = &stateSquish;
	currState->Enter(this);
}

void Ball::State_Bounce::Enter(GameObject* object) {
	Ball* ball = static_cast<Ball*>(object);
	ball->sprite.PlayAnimation(static_cast<int>(Ball_Anim::None_Anim));
	ball->velocity.y = Ball::bounceVelocity;
}
void Ball::State_Bounce::Update(GameObject* object, double dt) {
	Ball* ball = static_cast<Ball*>(object);
	ball->velocity.y -= Engine::GetGameStateManager().GetComponent<Gravity>()->GetValue() * dt;
}
void Ball::State_Bounce::TestForExit(GameObject*) 
{}

void Ball::State_Squish::Enter(GameObject* object) {
	Ball* ball = static_cast<Ball*>(object);
	ball->sprite.PlayAnimation(static_cast<int>(Ball_Anim::Squish_Anim));
}
void Ball::State_Squish::Update(GameObject*, double) {}
void Ball::State_Squish::TestForExit(GameObject* object) {
	Ball* ball = static_cast<Ball*>(object);
	if (ball->sprite.IsAnimationDone() == true) {
		ball->ChangeState(&ball->stateBounce);
	}
}

GameObjectType Ball::GetObjectType()
{
	return GameObjectType::Ball;
}

std::string Ball::GetObjectTypeName()
{
	return "Ball";
}

bool Ball::CanCollideWith(GameObjectType collideAgainstType)
{
	if (collideAgainstType == GameObjectType::Floor)
	{
		return true;
	}
	return false;
}

void Ball::ResolveCollision(GameObject* collideWith)
{
	if (collideWith->GetObjectType() == GameObjectType::Floor)
	{
		if (velocity.y < 0.0)
		{
			SetPositionY(static_cast<RectCollision*>(collideWith->GetComponent<Collision>())->GetWorldCoorRect().Top());
			velocity.y = 0;
			ChangeState(&stateSquish);
		}
	}
}