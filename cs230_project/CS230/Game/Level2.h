/*--------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name: Level2.h
Purpose: Header file for Level 2
Project: CS230
Author: Haewon Shon
Creation date: 3/25/2020
-----------------------------------------------------------------*/

#pragma once
#include "../Engine/GameState.h" // GameState
#include "../Engine/Input.h" // Input
#include "../Engine/GameObjectManager.h" // GameObjectManager
#include "../Engine/TransformMatrix.h" // TransformMatrix
#include "Particle.h"

class Ship;

class Level2 : public GameState
{
public:
	Level2();

	void Load() override;
	void Update(double dt) override;
	void Unload() override;
	void Draw() override;

	std::string GetName() override { return "Level2"; }

private:
	TransformMatrix cameraMatrix;

	Ship* shipPtr;

	Input::InputKey levelReload;
	Input::InputKey levelNext;
	Input::InputKey debugCollision;
};
