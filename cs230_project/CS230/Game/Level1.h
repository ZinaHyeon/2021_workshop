/*--------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name: Level1.h
Purpose: Header file for Level 1
Project: CS230
Author: Haewon Shon
Creation date: 3/25/2020
-----------------------------------------------------------------*/
#pragma once

#include "../Engine/GameState.h" // GameState
#include "../Engine/GameObjectManager.h" // GameObjectManager
#include "../Engine/Input.h" // Input

class Hero;

class Level1 : public GameState
{
public:
	Level1();

	void Load() override;
	void Update(double dt) override;
	void Unload() override;
	void Draw() override;

	std::string GetName() override { return "Level1"; }

private:	
	Hero* heroPtr;

	double timer;
	int lives;

	Input::InputKey levelReload;
	Input::InputKey levelNext;
	Input::InputKey debugCollision;
};
	