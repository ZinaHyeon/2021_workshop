/*--------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name: Hero_Anims.h
Purpose: Header file for hero animation
Project: CS230
Author: Kevin Wright
Creation date: 5/7/2020
-----------------------------------------------------------------*/
#pragma once

enum class Hero_Anim {
    Hero_Idle_Anim,
    Hero_Run_Anim,
    Hero_Jump_Anim,
    Hero_Fall_Anim,
    Hero_Skid_Anim,
};