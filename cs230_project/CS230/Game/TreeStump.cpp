/*--------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name: TreeStump.cpp
Purpose: TreeStump source file
Project: CS230
Author: Kevin Wright
Creation date: 4/27/2020
-----------------------------------------------------------------*/

#include "TreeStump.h"
#include "GameObjectType.h" // GameObjectType::TreeStump

TreeStump::TreeStump(Vector2D pos, int size) : GameObject(pos) 
{
	switch (size) {
	case 1:
		sprite.Load("assets/treeStump1.spt", this);
		break;
	case 2:
		sprite.Load("assets/treeStump2.spt", this);
		break;
	case 3:
		sprite.Load("assets/treeStump3.spt", this);
		break;
	case 5:
		sprite.Load("assets/treeStump5.spt", this);
		break;
	}
}

GameObjectType TreeStump::GetObjectType()
{
	return GameObjectType::TreeStump;
}

std::string TreeStump::GetObjectTypeName()
{
	return "TreeStump";
}