/*--------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name: Hero.h
Purpose: Header file for Hero
Project: CS230
Author: Haewon Shon
Creation date: 4/1/2020
-----------------------------------------------------------------*/
#pragma once

#include "../Engine/BasicDataTypes.h" // Vector2D
#include "../Engine/Input.h" // Input
#include "../Engine/GameObject.h" // GameObject

enum class GameObjectType;

class Hero : public GameObject
{
private:
    class State_Idle : public State
    {
    public:
        virtual void Enter(GameObject* object) override;
        virtual void Update(GameObject* object, double dt) override;
        virtual void TestForExit(GameObject* object) override;
        std::string GetName() override { return "Idle"; }
    };
    class State_Running : public State
    {
    public:
        virtual void Enter(GameObject* object) override;
        virtual void Update(GameObject* object, double dt) override;
        virtual void TestForExit(GameObject* object) override;
        std::string GetName() override { return "Running"; }
    };
    class State_Jumping : public State
    {
    public:
        virtual void Enter(GameObject* object) override;
        virtual void Update(GameObject* object, double dt) override;
        virtual void TestForExit(GameObject* object) override;
        std::string GetName() override { return "Jumping"; }
    };
    class State_Falling : public State
    {
    public:
        virtual void Enter(GameObject* object) override;
        virtual void Update(GameObject* object, double dt) override;
        virtual void TestForExit(GameObject* object) override;
        std::string GetName() override { return "Falling"; }
    };
    class State_Skidding : public State
    {
    public:
        virtual void Enter(GameObject* object) override;
        virtual void Update(GameObject* object, double dt) override;
        virtual void TestForExit(GameObject* object) override;
        std::string GetName() override { return "Skidding"; }
    };

public:
    Hero(Vector2D startPos);
    void Update(double dt) override;
    void Draw(TransformMatrix cameraMatrix) override;

    void UpdateXVelocity(double dt);

    GameObjectType GetObjectType() override;
    std::string GetObjectTypeName() override;
    bool CanCollideWith(GameObjectType collideAgainstType) override;    
    void ResolveCollision(GameObject* collidedWith) override;

    bool IsDead();
private:
    static const double jumpVelocity;
    static const double xAccel;
    static const double xDrag;
    static const double maxXVelocity;
    static const double hurtTime;

    Input::InputKey moveLeftKey;
    Input::InputKey moveRightKey;
    Input::InputKey jumpKey;

    GameObject* standingOnObject;

    bool isFlipped;
    bool isDead;

    double hurtTimer;
    bool drawHero;

    State_Idle stateIdle;
    State_Running stateRunning;
    State_Jumping stateJumping;
    State_Falling stateFalling;
    State_Skidding stateSkidding;
};
