/*--------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name: Score.h
Project: CS230
Purpose: Header file for the score component
Author: Haewon Shon
Creation date: 6/18/2020
-----------------------------------------------------------------*/

#pragma once
#include "..\Engine\Component.h" 

class Score : public Component {
public:
    Score() : score(0) {}
    void AddValue(int value) { score += value; }
    int GetValue() { return score; }
private:
    int score;
};