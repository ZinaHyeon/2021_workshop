/*--------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name: Splash.cpp
Purpose: Source file for Splash
Project: CS230
Author: Haewon Shon
Creation date: 3/25/2020
-----------------------------------------------------------------*/

#include "Splash.h"
#include "Screens.h" // LEVEL1
#include "../Engine//Engine.h" // Engine::GetLogger

void Splash::Load()
{
	timer = 3.0;
	std::string filepath = "assets/DigiPen_BLACK_1024px.png";
	dpLogo = Engine::GetTextureManager().Load(filepath);
}

void Splash::Update(double dt)
{
	timer -= dt;
	if (timer < 0.0)
	{
		Engine::GetGameStateManager().SetNextState(LEVEL1);
	}
}

void Splash::Unload()
{}

void Splash::Draw()
{
	Engine::GetWindow().Clear(0xFFFFFFFF);
	dpLogo->Draw(TranslateMatrix({ (Engine::GetWindow().GetSize() - dpLogo->GetSize()) / 2 }), { 0, 0 }, dpLogo->GetSize());
}