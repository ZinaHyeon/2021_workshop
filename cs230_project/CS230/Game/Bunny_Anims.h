/*--------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name: Ball_Anims.h
Purpose: Header file for ball animation
Project: CS230
Author: Kevin Wright
Creation date: 5/7/2020
-----------------------------------------------------------------*/
#pragma once

enum class Bunny_Anim {
    None_Anim,
    Dead_Anim,
};