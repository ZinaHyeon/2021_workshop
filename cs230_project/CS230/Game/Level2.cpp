/*--------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name: Level2.cpp
Purpose: Source file for Level2
Project: CS230
Author: Haewon Shon
Creation date: 3/25/2020
-----------------------------------------------------------------*/

#include "Level2.h"
#include "../Engine//Engine.h" // Engine::GetLogger
#include "Ship.h" // Ship
#include "Meteor.h" // Meteor
#include "Score.h"
#include "EnemyShip.h"
#include "../Engine/Camera.h"

Level2::Level2() : levelReload(Input::KeyboardButton::R), levelNext(Input::KeyboardButton::Enter), debugCollision(Input::KeyboardButton::Escape)
{}

void Level2::Load() {
	Vector2D windowSize = Engine::GetWindow().GetSize();
	shipPtr = new Ship({ windowSize / 2.0 });
	Engine::GetGameObjectManager().Add(new Meteor());
	Engine::GetGameObjectManager().Add(new Meteor());
	Engine::GetGameObjectManager().Add(new Meteor());
	Engine::GetGameObjectManager().Add(new Meteor());
	Engine::GetGameObjectManager().Add(new Meteor());
	Engine::GetGameObjectManager().Add(shipPtr);
	Engine::GetGameObjectManager().Add(new EnemyShip(shipPtr));

	components.AddComponent(new Score());
	components.AddComponent(new Camera(nullptr, { 0, 0 }));
	components.AddComponent(new HitParticles());
	components.AddComponent(new MeteorBitsParticles());
}

void Level2::Update(double dt)
{
	Engine::GetGameObjectManager().UpdateAll(dt);

	if (levelNext.IsKeyRelease() == true)
	{
		Engine::GetGameStateManager().Shutdown();
	}

	if (levelReload.IsKeyRelease() == true)
	{
		Engine::GetGameStateManager().ReloadState();
	}

	if (debugCollision.IsKeyRelease() == true)
	{
		Engine::Instance().ShowCollision(!Engine::Instance().ShowCollision());
	}
}

void Level2::Unload()
{
	components.Clear();
}

void Level2::Draw()
{
	Color clearColor = Engine::Instance().ShowCollision() == true ? 0xF4C0FFFF : 0x000000FF;
	Engine::GetWindow().Clear(clearColor);
	Engine::GetGameObjectManager().DrawAll(cameraMatrix);

	int score = Engine::GetGameStateManager().GetComponent<Score>()->GetValue();

	Vector2DInt winSize = Engine::GetWindow().GetSize();
	std::string scoreString = "Score: " + std::to_string(score / 100) + std::to_string((score % 100) / 10) + std::to_string(score % 10);
	Engine::Instance().DrawText(0, { 10, winSize.y - 75 }, scoreString);

	if (shipPtr->IsDead() == true) {
		Engine::Instance().DrawText(0, winSize / 2 + Vector2DInt{ 0, 30 }, "Game Over", SpriteFont::Justified::CenterX);
		Engine::Instance().DrawText(0, winSize / 2 + Vector2DInt{ 0, -30 }, "Press r to restart", SpriteFont::Justified::CenterX);
	}
}