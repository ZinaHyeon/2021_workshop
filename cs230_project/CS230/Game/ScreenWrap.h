/*--------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name: ScreenWrap.h
Project: CS230
Purpose: Header file for the screenwrap component
Author: Haewon Shon
Creation date: 6/18/2020
-----------------------------------------------------------------*/

#pragma once
#include "..\Engine\Component.h" 

class GameObject;

class ScreenWrap : public Component {
public:
    ScreenWrap(GameObject* object);
    void Wrap();
private:
    GameObject* objectToWrap;
};