/*--------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name: Ship.cpp
Purpose: Source file for Ship
Project: CS230
Author: Haewon Shon
Creation date: 4/1/2020
-----------------------------------------------------------------*/

#include "../Engine/Engine.h" // Engine::GetWindow
#include "Meteor.h"
#include "GameObjectType.h" //GameObjectType::Meteor
#include "Meteor_Anims.h"
#include "../Engine/GameState.h"
#include "../Engine/Collision.h"
#include "Particle.h"
#include "Level2_ParticleTypes.h" // Level2_particles meteorBits, hit
#include "Score.h"
#include "ScreenWrap.h"

constexpr double PI = 3.1415926535897932384626433832795;

Meteor::Meteor() : GameObject({ static_cast<double>(rand() % static_cast<int> (Engine::GetWindow().GetSize().x)),
								static_cast<double>(rand() % static_cast<int> (Engine::GetWindow().GetSize().y)) },
	((rand() % static_cast<int>(PI * 2 * 1024)) / 1024.0f), { 1, 1 }) , size(1), health(100), isDead(false) {
	sprite.Load("assets/Meteor.spt", this);

	velocity.x = 100 - static_cast<double> (rand() % 200);
	velocity.y = 100 - static_cast<double> (rand() % 200);

	double scaleAmount = (rand() % 100) / 400.0 + .75;
	SetScale({ scaleAmount, scaleAmount });

	components.AddComponent(new ScreenWrap(this));
}

Meteor::Meteor(Meteor& original) : GameObject(original.GetPosition(), GetRotation(), { 1,1 }), size(original.size + 1), health(100), isDead(false)
{
	sprite.Load("assets/Meteor.spt", this);
	double scaleAmount = 1;

	switch (size) 
	{
	case 2:
		scaleAmount = (rand() % 100) / 400.0 + .50;
		break;
	case 3:
		scaleAmount = (rand() % 100) / 400.0 + .25;
		break;
	}
	SetScale({ scaleAmount, scaleAmount });
	components.AddComponent(new ScreenWrap(this));
}

void Meteor::Update(double dt)
{
	GameObject::Update(dt);
	components.GetComponent<ScreenWrap>()->Wrap();
	if (sprite.IsAnimationDone() == true)
	{
		GameObject::Destroy();
	}
}

GameObjectType Meteor::GetObjectType()
{
	return GameObjectType::Meteor;
}

std::string Meteor::GetObjectTypeName()
{
	return "Meteor";
}

bool Meteor::CanCollideWith(GameObjectType collideAgainstType)
{
	if (collideAgainstType == GameObjectType::Laser)
	{
		return true;
	}
	else
	{
		return false;
	}
}

void Meteor::ResolveCollision(GameObject* collideWith)
{
	if (isDead == true)
	{
		return;
	}

	if (collideWith->GetObjectType() == GameObjectType::Laser)
	{
		health -= 10;
		Engine::GetGameStateManager().GetComponent<Score>()->AddValue(20);
		if (health <= 0)
		{
			if (size < 3)
			{
				Meteor* newMeteor1 = new Meteor(*this);
				Meteor* newMeteor2 = new Meteor(*this);
				newMeteor1->velocity = (RotateMatrix(PI / 6) * velocity);
				newMeteor2->velocity = (RotateMatrix(-PI / 6) * velocity);
				Engine::GetGameObjectManager().Add(newMeteor1);
				Engine::GetGameObjectManager().Add(newMeteor2);
			}
			sprite.PlayAnimation(static_cast<int>(Meteor_Anim::FadeOut_Anim));
			isDead = true;
			Engine::GetGameStateManager().GetComponent<Score>()->AddValue(100);
		}
		else 
		{
			velocity += collideWith->GetVelocity() * 0.01;
			Vector2D emitVector = (collideWith->GetPosition() - GetPosition()).Normalize();
			Vector2D emitterPosition = GetPosition() + emitVector * GetComponent<CircleCollision>()->GetRadius();
			
			Engine::GetGameStateManager().GetComponent<MeteorBitsParticles>()->Emit(5, emitterPosition, velocity, emitVector * 50, PI / 3);
			Engine::GetGameStateManager().GetComponent<HitParticles>()->Emit(1, emitterPosition, velocity, { 0,0 }, 0);
		}

		collideWith->Destroy();
	}
}