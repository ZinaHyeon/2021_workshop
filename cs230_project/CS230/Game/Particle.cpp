/*--------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name: Particle.cpp
Purpose: Particle source file
Project: CS230
Author: Haewon Shon
Creation date: 6/9/2020
-----------------------------------------------------------------*/

#include "Particle.h"
#include "../Engine/Engine.h" // GetGameObjectManager
#include "GameObjectType.h"

ParticleEmitter::ParticleEmitter(std::string spriteFile, int totalParticles, double lifetime)
	: numParticles(totalParticles), lifetime(lifetime), particleToUse(0)
{
	for (int i = 0; i < numParticles; ++i)
	{
		Particle* newParticle = new Particle(spriteFile);
		particles.push_back(newParticle);
		Engine::GetGameObjectManager().Add(newParticle);
	}
}

void ParticleEmitter::Emit(int toEmit, Vector2D emitterPosition, Vector2D emitterVelocity, Vector2D emitVector, double spread)
{
	for (int i = 0; i < toEmit; ++i)
	{
		if (particles[particleToUse]->IsAlive() == true)
		{
			Engine::GetLogger().LogError("Particle is being overwritten");
		}
		double angleVariation = spread != 0 ? ((rand() % static_cast<int>(spread * 1024)) / 1024.0f) - spread / 2 : 0;
		Vector2D particleVelocity = RotateMatrix(angleVariation) * emitVector + emitterVelocity;
		particles[particleToUse]->Revive(emitterPosition, particleVelocity, lifetime);

		if (++particleToUse >= numParticles)
		{
			particleToUse = 0;
		}
	}
}

void ParticleEmitter::Clear()
{
	for (Particle* particle : particles)
	{
		particle->Destroy();
	}
	particles.clear();
}

ParticleEmitter::Particle::Particle(std::string spriteFile)
	: GameObject({ 0, 0 }), life(0.0)
{
	sprite.Load(spriteFile, this);
}

void ParticleEmitter::Particle::Revive(Vector2D newPosition, Vector2D newVelocity, double lifetime)
{
	SetPosition(newPosition);
	SetVelocity(newVelocity);
	life = lifetime;
	sprite.PlayAnimation(0);
}

void ParticleEmitter::Particle::Update(double dt)
{
	if (IsAlive() == true)
	{
		life -= dt;
		GameObject::Update(dt);
	}
}

void ParticleEmitter::Particle::Draw(TransformMatrix matrix)
{
	if (IsAlive() == true)
	{
		GameObject::Draw(matrix);
	}
}

GameObjectType ParticleEmitter::Particle::GetObjectType()
{
	return GameObjectType::Particle;
}

HitParticles::HitParticles() : ParticleEmitter("assets/hit.spt", 6, 1) { }

MeteorBitsParticles::MeteorBitsParticles() : ParticleEmitter("assets/particle.spt", 30, 1) { };