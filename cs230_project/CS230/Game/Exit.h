/*--------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name: Exit.h
Purpose: Exit header file
Project: CS230
Author: Haewon Shon
Creation date: 6/7/2020
-----------------------------------------------------------------*/
#pragma once
#include "..\Engine\GameObject.h"

class Exit : public GameObject {
public:
	Exit(RectInt rect);
	void Update(double) override;
	void Draw(TransformMatrix displayMatrix) override;
	std::string GetObjectTypeName() override;
	virtual GameObjectType GetObjectType() override;
	void ResolveCollision(GameObject*) override;
private:
};