/*--------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name: Gravity.h
Project: CS230
Purpose: Header file for the gravity component
Author: Kevin Wright
Creation date: 6/6/2020
-----------------------------------------------------------------*/

#pragma once
#include "..\Engine\Component.h" 

class Gravity : public Component {
public:
    Gravity(double value) : gravity(value) {}
    double GetValue() { return gravity; }
private:
    const double gravity;
};