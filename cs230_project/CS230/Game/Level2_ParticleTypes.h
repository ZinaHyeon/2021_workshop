/*--------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name: Level2_ParticleTypes.h
Purpose: Particle Type header file for Level 2
Project: CS230
Author: Kevin Wright
Creation date: 5/19/2020
-----------------------------------------------------------------*/
#pragma once

enum class Level2_Particles 
{
    MeteorBits,
    Hit,
};