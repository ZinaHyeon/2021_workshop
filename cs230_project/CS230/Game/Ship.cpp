/*--------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name: Ship.cpp
Purpose: Source file for Ship
Project: CS230
Author: Haewon Shon
Creation date: 4/1/2020
-----------------------------------------------------------------*/

#include "Ship.h"
#include "Flame_Anims.h"
#include "Ship_Anims.h"
#include "../Engine/Engine.h" // Engine::GetLogger
#include "../Engine/TransformMatrix.h" // TransformMatrix
#include "../Engine/Collision.h" // collision
#include "GameObjectType.h" // GameObjectType::Ship
#include "Laser.h"
#include "ScreenWrap.h"
#include "EnemyShip.h"

const double Ship::acceleration = 300.0;
const double Ship::maxVelocity = 500.0;
const double Ship::drag = 0.9;
const double Ship::rotationRate = 2.0;

Ship::Ship(Vector2D startPos)
	: GameObject(startPos, 0.0, { 0.75, 0.75 }), 
	rotateCounterKey(Input::KeyboardButton::A), rotateClockKey(Input::KeyboardButton::D), 
	accelerateKey(Input::KeyboardButton::W),fireLazerKey(Input::KeyboardButton::Space)
{
	isAccelerating = false;
	sprite.Load("assets/Ship.spt", this);
	sprite.PlayAnimation(static_cast<int>(Flame_Anim::None_Anim));

	flame1.Load("assets/flame.spt", this);
	flame2.Load("assets/flame.spt", this);
	flame1.PlayAnimation(static_cast<int>(Flame_Anim::None_Anim));
	flame2.PlayAnimation(static_cast<int>(Flame_Anim::None_Anim));

	isDead = false;

	components.AddComponent(new ScreenWrap(this));
}

void Ship::Update(double dt)
{
	if (isDead == false)
	{
		bool isRotateCounterKeyDown = rotateCounterKey.IsKeyDown();
		bool isRotateClockKeyDown = rotateClockKey.IsKeyDown();
		bool isAccelerateKeyDown = accelerateKey.IsKeyDown();

		if (isRotateCounterKeyDown != isRotateClockKeyDown)
		{
			if (isRotateCounterKeyDown == true)
			{
				UpdateRotation(rotationRate * dt);
			}
			else if (isRotateClockKeyDown == true)
			{
				UpdateRotation(-rotationRate * dt);
			}
		}

		if (isAccelerateKeyDown == true)
		{
			if (isAccelerating == false)
			{
				isAccelerating = true;
				flame1.PlayAnimation(static_cast<int>(Flame_Anim::Flame_Anim));
				flame2.PlayAnimation(static_cast<int>(Flame_Anim::Flame_Anim));
			}
			velocity += RotateMatrix(GetRotation()) * Vector2D { 0, acceleration* dt };
		}
		else if (accelerateKey.IsKeyRelease() == true)
		{
			isAccelerating = false;
			flame1.PlayAnimation(static_cast<int>(Flame_Anim::None_Anim));
			flame2.PlayAnimation(static_cast<int>(Flame_Anim::None_Anim));
		}

		if (fireLazerKey.IsKeyRelease() == true) {
			Engine::GetGameObjectManager().Add(new Laser(GetMatrix() * sprite.GetHotSpot(3), GetRotation(), GetScale(), RotateMatrix(GetRotation()) * Vector2D { 0, 600 }));
			Engine::GetGameObjectManager().Add(new Laser(GetMatrix() * sprite.GetHotSpot(4), GetRotation(), GetScale(), RotateMatrix(GetRotation()) * Vector2D { 0, 600 }));
		}
	}

	double trueVelocity = std::sqrt(velocity.LengthSquared());
	if (trueVelocity > maxVelocity) 
	{
		velocity = velocity.Normalize() * maxVelocity;
	}

	velocity -= (velocity * Ship::drag * dt);
	UpdatePosition(velocity * dt);

	sprite.Update(dt);
	flame1.Update(dt);
	flame2.Update(dt);

	components.GetComponent<ScreenWrap>()->Wrap();
}

void Ship::Draw(TransformMatrix cameraMatrix)
{
	TransformMatrix displayMatrix = cameraMatrix * GetMatrix();
	sprite.Draw(displayMatrix);
	flame1.Draw(displayMatrix * TranslateMatrix(sprite.GetHotSpot(1)));
	flame2.Draw(displayMatrix * TranslateMatrix(sprite.GetHotSpot(2)));

	if (Engine::Instance().ShowCollision() == true && GetComponent<Collision>() != nullptr)
	{
		GetComponent<Collision>()->Draw(displayMatrix);
	}
}

GameObjectType Ship::GetObjectType()
{
	return GameObjectType::Ship;
}

std::string Ship::GetObjectTypeName()
{
	return "Ship";
}

bool Ship::CanCollideWith(GameObjectType collideAgainstType)
{
	if (collideAgainstType == GameObjectType::Particle)
	{
		return false;
	}
	else
	{
		return true;
	}
}

void Ship::ResolveCollision(GameObject* collidedWith)
{
	if (isDead == true)
	{
		return;
	}

	if (collidedWith->GetObjectType() == GameObjectType::Meteor)
	{
		sprite.PlayAnimation(static_cast<int>(Ship_Anim::Explosion_Anim));
		flame1.PlayAnimation(static_cast<int>(Flame_Anim::None_Anim));
		flame2.PlayAnimation(static_cast<int>(Flame_Anim::None_Anim));
		isDead = true;
	}
	else if (collidedWith->GetObjectType() == GameObjectType::EnemyShip)
	{
		if (reinterpret_cast<EnemyShip*>(collidedWith)->IsDead() == true)
		{
			return;
		}
		sprite.PlayAnimation(static_cast<int>(Ship_Anim::Explosion_Anim));
		flame1.PlayAnimation(static_cast<int>(Flame_Anim::None_Anim));
		flame2.PlayAnimation(static_cast<int>(Flame_Anim::None_Anim));
		isDead = true;
	}
}

bool Ship::IsDead()
{
	return isDead;
}