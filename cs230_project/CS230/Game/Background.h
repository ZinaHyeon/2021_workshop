/*--------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name: Background.h
Purpose: Header file for Background component
Project: CS230
Author: Haewon Shon
Creation date: 4/11/2020
-----------------------------------------------------------------*/
#pragma once
#include <string> // std::string
#include <vector> // std::vector
#include "..\Engine\BasicDataTypes.h" // Vector2D, Rect
#include "..\Engine\Component.h" 

class Texture;

class Background : public Component 
{
public:
    Background();
    void Add(const std::string& texturePath, int level);
    void Unload();
    void Draw(Vector2D camera);
private:
    struct ParallaxInfo {
        Texture *texturePtr;
        int level;
    };
    std::vector<ParallaxInfo> backgrounds;
};
