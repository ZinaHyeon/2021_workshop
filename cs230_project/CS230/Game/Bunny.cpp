/*--------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name: TreeStump.cpp
Purpose: TreeStump source file
Project: CS230
Author: Kevin Wright
Creation date: 4/27/2020
-----------------------------------------------------------------*/

#include "Bunny.h"
#include "GameObjectType.h"
#include "Bunny_Anims.h"
#include "../Engine/Engine.h"
#include "Score.h"

Bunny::Bunny(Vector2D pos, double patrolMinX, double patrolMaxX)
	: GameObject(pos), patrolMinX(patrolMinX), patrolMaxX(patrolMaxX)
{
	sprite.Load("assets/bunny.spt", this);
	isDead = false;
	
	sprite.PlayAnimation(static_cast<int>(Bunny_Anim::None_Anim));

	xVelocity = 90.0;
}

void Bunny::Update(double dt)
{
	GameObject::Update(dt);

	if (isDead == true)
	{
		return;
	}

	double newXPos = GetPosition().x + xVelocity * dt;

	// move to right
	if (GetScale().x == 1.0)
	{
		if (newXPos > patrolMaxX)
		{
			SetPositionX(patrolMaxX);
			SetScale({ -1.0, 1.0 });
			xVelocity = -xVelocity;
		}
		else
		{
			SetPositionX(newXPos);
		}
	}
	// move to left
	else
	{
		if (newXPos < patrolMinX)
		{
			SetPositionX(patrolMinX);
			SetScale({ 1.0, 1.0 });
			xVelocity = -xVelocity;
		}
		else
		{
			SetPositionX(newXPos);
		}
	}
}

GameObjectType Bunny::GetObjectType()
{
	if (isDead == true)
	{
		return GameObjectType::DeadBunny;
	}
	else
	{
		return GameObjectType::Bunny;
	}
}

std::string Bunny::GetObjectTypeName()
{
	return "Bunny";
}

void Bunny::ResolveCollision(GameObject* collideWith)
{
	if (collideWith->GetObjectType() == GameObjectType::Hero)
	{
		isDead = true;
		sprite.PlayAnimation(static_cast<int>(Bunny_Anim::Dead_Anim));
		Engine::GetGameStateManager().GetComponent<Score>()->AddValue(100);
	}
}