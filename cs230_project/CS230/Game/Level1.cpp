/*--------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name: Level1.cpp
Purpose: Source file for Level1
Project: CS230
Author: Haewon Shon
Creation date: 3/25/2020
-----------------------------------------------------------------*/

#include "Level1.h"
#include "Screens.h" // LEVEL2
#include "../Engine//Engine.h" // Engine::GetLogger
#include "../Engine/Camera.h"
#include "Hero.h"
#include "Ball.h"
#include "TreeStump.h"
#include "Bunny.h"
#include "Floor.h"
#include "Exit.h"
#include "Gravity.h"
#include "Score.h"
#include "Background.h"
 
Level1::Level1()
	: lives(3), levelReload(Input::KeyboardButton::R), levelNext(Input::KeyboardButton::Enter), debugCollision(Input::KeyboardButton::Escape) {}

void Level1::Load() 
{
	timer = 90.0;
	double floor = 127.0;

	Engine::GetGameObjectManager().Add(new Ball({ 600, floor }));
	Engine::GetGameObjectManager().Add(new Ball({ 2700, floor }));
	Engine::GetGameObjectManager().Add(new Ball({ 4800, floor }));
	Engine::GetGameObjectManager().Add(new Bunny({ 1000, floor }, 674, 1132));
	Engine::GetGameObjectManager().Add(new Bunny({ 2000, floor }, 1635, 2135));
	Engine::GetGameObjectManager().Add(new Bunny({ 3200, floor }, 2860, 4250));
	Engine::GetGameObjectManager().Add(new Bunny({ 3800, floor }, 2860, 4250));
	Engine::GetGameObjectManager().Add(new TreeStump({ 300, floor }, 3));
	Engine::GetGameObjectManager().Add(new TreeStump({ 1200, floor }, 2));
	Engine::GetGameObjectManager().Add(new TreeStump({ 2200, floor }, 1));
	Engine::GetGameObjectManager().Add(new TreeStump({ 2800, floor }, 5));
	Engine::GetGameObjectManager().Add(new TreeStump({ 5100, floor }, 5));
	Engine::GetGameObjectManager().Add(new Floor({ {0, 0}, {1471, static_cast<int>(floor)} }));
	Engine::GetGameObjectManager().Add(new Floor({ {1636, 0}, {4262, static_cast<int>(floor)} }));
	Engine::GetGameObjectManager().Add(new Floor({ {4551, 0}, {5760, static_cast<int>(floor)} }));
	heroPtr = new Hero({ 100, floor - 0.1 });
	Engine::GetGameObjectManager().Add(heroPtr);
	Engine::GetGameObjectManager().Add(new Exit({ {5550, static_cast<int>(floor)}, {5760, 683} }));

	components.AddComponent(new Gravity(1400));
	components.AddComponent(new Score());
	components.AddComponent(new Background());
	components.AddComponent(new Camera(heroPtr, { 0, 0 }));

	Engine::GetGameStateManager().GetComponent<Background>()->Add("assets/clouds.png", 4);
	Engine::GetGameStateManager().GetComponent<Background>()->Add("assets/mountains.png", 2);
	Engine::GetGameStateManager().GetComponent<Background>()->Add("assets/foreground.png", 1);
}

void Level1::Update(double dt)
{
	timer -= dt;
	if (timer <= 0.0 || heroPtr->IsDead() == true)
	{
		if (--lives == 0)
		{
			Engine::GetGameStateManager().SetNextState(LEVEL2);
		}
		else
		{
			Engine::GetGameStateManager().ReloadState();
		}
	}

	Engine::GetGameObjectManager().UpdateAll(dt);

	if (levelNext.IsKeyRelease() == true)
	{
		Engine::GetGameStateManager().SetNextState(LEVEL2);
	}

	if (levelReload.IsKeyRelease() == true)
	{
		Engine::GetGameStateManager().ReloadState();
	}

	if (debugCollision.IsKeyRelease() == true)
	{
		Engine::Instance().ShowCollision(!Engine::Instance().ShowCollision());
	}

	Engine::GetGameStateManager().GetComponent<Camera>()->Update();
}

void Level1::Unload()
{ 
	components.Clear();
	heroPtr = nullptr;
}

void Level1::Draw()
{
	Engine::GetWindow().Clear(0x3399DAFF);
	Engine::GetGameStateManager().GetComponent<Background>()->Draw(Engine::GetGameStateManager().GetComponent<Camera>()->GetCameraPosition());
	Engine::GetGameObjectManager().DrawAll(Engine::GetGameStateManager().GetComponent<Camera>()->GetCameraMatrix());

	int score = Engine::GetGameStateManager().GetComponent<Score>()->GetValue();

	Vector2DInt winSize = Engine::GetWindow().GetSize();
	Engine::Instance().DrawText(0, { 10, winSize.y - 75 }, "Lives: " + std::to_string(lives));
	std::string scoreString = "Score: " + std::to_string(score / 100) + std::to_string((score % 100) / 10) + std::to_string(score % 10);
	Engine::Instance().DrawText(0, { winSize.x / 2, winSize.y - 75 }, scoreString, SpriteFont::Justified::CenterX);
	Engine::Instance().DrawText(0, { winSize.x - 10, winSize.y - 75 }, "Time: " + std::to_string(static_cast<int>(timer)), SpriteFont::Justified::Right);
}