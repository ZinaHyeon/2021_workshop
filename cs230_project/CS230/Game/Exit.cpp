/*--------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name: Floor.cpp
Purpose: Floor source file
Project: CS230
Author: Kevin Wright
Creation date: 5/4/2020
-----------------------------------------------------------------*/

#include "..\Engine\Collision.h"
#include "..\Engine\BasicDataTypes.h"   //RectInt
#include "GameObjectType.h"
#include "Exit.h"
#include "Screens.h" // LEVEL2
#include "../Engine/Engine.h" // GetGameStateManager

Exit::Exit(RectInt rect) : GameObject(rect.bottomLeft) {
	AddComponent(new RectCollision({ { 0, 0 }, rect.Size() }, this));
}

void Exit::Update(double) {}

void Exit::Draw(TransformMatrix) {}

std::string Exit::GetObjectTypeName() {
	return "Exit";
}

GameObjectType Exit::GetObjectType() {
	return GameObjectType::Exit;
}

void Exit::ResolveCollision(GameObject*)
{
	Engine::GetGameStateManager().SetNextState(LEVEL2);
}