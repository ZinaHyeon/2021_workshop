/*--------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name: Background.cpp
Purpose: Source file for Background
Project: CS230
Author: Haewon Shon
Creation date: 4/11/2020
-----------------------------------------------------------------*/

#include "Background.h"
#include "../Engine/Engine.h" // Engine::GetWindow().GetSize()
#include "../Engine/Camera.h" // Camera::SetCameraRange

Background::Background()
{
	backgrounds.clear();
}

void Background::Add(const std::string& texturePath, int level)
{
	Texture* texturePtr = Engine::GetTextureManager().Load(texturePath);
	backgrounds.push_back({ texturePtr, level });

	if (level == 1)
	{
		RectInt cameraRange;
		cameraRange.bottomLeft = { 0, 0 };
		cameraRange.topRight = backgrounds.back().texturePtr->GetSize() - Engine::GetWindow().GetSize();
		Engine::GetGameStateManager().GetComponent<Camera>()->SetCameraRange(cameraRange);
	}
}

void Background::Unload()
{
	backgrounds.clear();
}

void Background::Draw(Vector2D camera)
{
	for (ParallaxInfo& levelInfo : backgrounds)
	{
		levelInfo.texturePtr->Draw(TranslateMatrix(Vector2D{ -camera.x / levelInfo.level, -camera.y }), { 0, 0 }, levelInfo.texturePtr->GetSize());
	} 
}