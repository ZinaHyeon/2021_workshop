/*--------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name: Hero.h
Purpose: Header file for Hero
Project: CS230
Author: Haewon Shon
Creation date: 4/12/2020
-----------------------------------------------------------------*/
#pragma once

#include "../Engine/BasicDataTypes.h" // Vector2D
#include "../Engine/GameObject.h" // GameObject

enum class GameObjectType;

class Ball : public GameObject
{
public:
    Ball(Vector2D startPos);

    GameObjectType GetObjectType() override;
    std::string GetObjectTypeName() override;
    bool CanCollideWith(GameObjectType collideAgainstType) override;
    void ResolveCollision(GameObject* collideWith) override;
private:
    class State_Bounce : public State {
        void Enter(GameObject* object) override;
        void Update(GameObject* object, double dt) override;
        void TestForExit(GameObject* object) override;
        std::string GetName() override { return "bounce"; }
    };
    class State_Squish : public State {
        void Enter(GameObject* object) override;
        void Update(GameObject* object, double dt) override;
        void TestForExit(GameObject* object) override;
        std::string GetName() override { return "squish"; }
    };
    State_Bounce stateBounce;
    State_Squish stateSquish;

    static const double bounceVelocity;

    bool jumping;
};
