/*--------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name: TreeStump.h
Purpose: TreeStump header file
Project: CS230
Author: Kevin Wright
Creation date: 4/27/2020
-----------------------------------------------------------------*/

#pragma once

#include "..\Engine\GameObject.h"

enum class GameObjectType;

class Bunny : public GameObject {
public:
	Bunny(Vector2D pos, double patrolMinX, double patrolMaxX);
	void Update(double dt) override;
	GameObjectType GetObjectType() override;
	std::string GetObjectTypeName() override;
	void ResolveCollision(GameObject* collidedWith) override;
private:
	double xVelocity;
	bool isDead;
	double patrolMinX;
	double patrolMaxX;
};