/*--------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name: ScreenWrap.cpp
Project: CS230
Purpose: Source file for the screenwrap component
Author: Haewon Shon
Creation date: 6/18/2020
-----------------------------------------------------------------*/

#include "ScreenWrap.h"
#include "../Engine/BasicDataTypes.h" // Vector2D
#include "../Engine/Engine.h"
#include "../Engine/Sprite.h"
#include "../Engine/GameObject.h"

ScreenWrap::ScreenWrap(GameObject* object) : objectToWrap(object) {}

void ScreenWrap::Wrap()
{
	Vector2D windowSize = Engine::GetWindow().GetSize();
  	Vector2D objectSize = objectToWrap->GetSprite().GetFrameSize();

	if (objectToWrap->GetPosition().x > windowSize.x + objectSize.x / 2.0)
	{
		objectToWrap->SetPositionX(-objectSize.x / 2.0);
	}
	else if (objectToWrap->GetPosition().x < -objectSize.x / 2.0)
	{
		objectToWrap->SetPositionX(windowSize.x + objectSize.x / 2.0);
	}

	if (objectToWrap->GetPosition().y > windowSize.y + objectSize.y / 2.0)
	{
		objectToWrap->SetPositionY(-objectSize.y / 2.0);
	}
	else if (objectToWrap->GetPosition().y < -objectSize.y / 2.0)
	{
		objectToWrap->SetPositionY(windowSize.y + objectSize.y / 2.0);
	}
}