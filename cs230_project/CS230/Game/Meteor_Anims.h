/*--------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name: Meteor_Anims.h
Purpose: Header file for ball animation
Project: CS230
Author: Haewon Shon
Creation date: 6/9/2020
-----------------------------------------------------------------*/
#pragma once

enum class Meteor_Anim {
    None_Anim,
    FadeOut_Anim,
};