/*--------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name: Hero.cpp
Purpose: Source file for Hero
Project: CS230
Author: Haewon Shon
Creation date: 4/1/2020
-----------------------------------------------------------------*/

#include "Hero.h"
#include "Level1.h" // Level1::floor
#include "../Engine/Engine.h" // Engine::GetLogger
#include "Hero_Anims.h"
#include "GameObjectType.h" // GameObjectType::Hero
#include "../Engine/Collision.h" // RectCollision
#include "Gravity.h"

const double Hero::jumpVelocity = 800.0;
const double Hero::xAccel = 500.0;
const double Hero::xDrag = 750.0;
const double Hero::maxXVelocity = 750.0;
const double Hero::hurtTime = 2.0;

Hero::Hero(Vector2D startPos)
	: GameObject(startPos), moveLeftKey(Input::KeyboardButton::Left), moveRightKey(Input::KeyboardButton::Right), jumpKey(Input::KeyboardButton::Up)
{
	sprite.Load("assets/Hero.spt", this);
	isFlipped = false;
	currState = &stateIdle;
	drawHero = true;
	hurtTimer = 0.0;

	for (GameObject* objectB : Engine::GetGameObjectManager())
	{
		if (objectB->GetObjectType() == GameObjectType::Floor && DoesCollideWith(objectB))
		{
			SetPositionY(static_cast<RectCollision*>(objectB->GetComponent<Collision>())->GetWorldCoorRect().Top());
			standingOnObject = objectB;
		}
	}
	currState->Enter(this);
}

void Hero::Update(double dt)
{
	if (hurtTimer > 0.0)
	{
		hurtTimer -= dt;
		drawHero = !drawHero;
		if (hurtTimer < 0.0)
		{
			drawHero = true;
		}
	}
	GameObject::Update(dt);

	if (isFlipped == true)
	{
		SetScale({ -1.0, 1.0 });
	}
	else
	{
		SetScale({ 1.0, 1.0 });
	}
}

void Hero::Draw(TransformMatrix cameraMatrix)
{
	if (drawHero == true)
	{
		GameObject::Draw(cameraMatrix);
	}
}

void Hero::UpdateXVelocity(double dt)
{
	bool isLeftKeyDown = moveLeftKey.IsKeyDown();
	bool isRightKeyDown = moveRightKey.IsKeyDown();

	if (isLeftKeyDown != isRightKeyDown)
	{
		if (isLeftKeyDown == true)
		{
			isFlipped = true;
			velocity.x -= xAccel * dt;
			if (velocity.x < -Hero::maxXVelocity)
			{
				velocity.x = -Hero::maxXVelocity;
			}
		}
		else if (isRightKeyDown == true)
		{
			isFlipped = false;
			velocity.x += xAccel * dt;
			if (velocity.x > Hero::maxXVelocity)
			{
				velocity.x = Hero::maxXVelocity;
			}
		}
	}
	else
	{
		if (velocity.x > 0.0)
		{
			velocity.x -= xDrag * dt;
			if (velocity.x < 0.0)
			{
				velocity.x = 0.0;
			}
		}
		else if (velocity.x < 0.0)
		{
			velocity.x += xDrag * dt;
			if (velocity.x > 0.0)
			{
				velocity.x = 0.0;
			}
		}
	}
}

void Hero::State_Idle::Enter(GameObject* object)
{
	Hero* hero = static_cast<Hero*>(object);
	hero->sprite.PlayAnimation(static_cast<int>(Hero_Anim::Hero_Idle_Anim));

	if (hero->standingOnObject == nullptr)
	{
		Engine::GetLogger().LogError("Hero standing on null");
	}
}

void Hero::State_Idle::Update(GameObject*, double)
{}

void Hero::State_Idle::TestForExit(GameObject* object)
{
	Hero* hero = static_cast<Hero*>(object);
	if (hero->moveLeftKey.IsKeyDown() == true || hero->moveRightKey.IsKeyDown() == true)
	{
		hero->ChangeState(&hero->stateRunning);
	}
	else if (hero->jumpKey.IsKeyDown() == true)
	{
		hero->ChangeState(&hero->stateJumping);
	}
}

void Hero::State_Running::Enter(GameObject* object)
{
	Hero* hero = static_cast<Hero*>(object);
	if (hero->moveLeftKey.IsKeyDown() == true)
	{
		hero->isFlipped = true;
	}
	else if (hero->moveRightKey.IsKeyDown() == true)
	{
		hero->isFlipped = false;
	}
	hero->sprite.PlayAnimation(static_cast<int>(Hero_Anim::Hero_Run_Anim));

	if (hero->standingOnObject == nullptr)
	{
		Engine::GetLogger().LogError("Hero standing on null");
	}
}

void Hero::State_Running::Update(GameObject* object, double dt)
{
	Hero* hero = static_cast<Hero*>(object);
	if (hero->velocity.x > 0 && hero->moveRightKey.IsKeyDown() == true) {
		hero->isFlipped = false;
	}
	if (hero->velocity.x < 0 && hero->moveLeftKey.IsKeyDown() == true) {
		hero->isFlipped = true;
	}
	hero->UpdateXVelocity(dt);
}

void Hero::State_Running::TestForExit(GameObject* object)
{
	Hero* hero = static_cast<Hero*>(object);
	if (hero->velocity.x > 0 && hero->moveLeftKey.IsKeyDown() == true)
	{
		hero->ChangeState(&hero->stateSkidding);
	}
	else if (hero->velocity.x < 0 && hero->moveRightKey.IsKeyDown() == true)
	{
		hero->ChangeState(&hero->stateSkidding);
	}
	else if (hero->velocity.x == 0.0)
	{
		hero->ChangeState(&hero->stateIdle);
	}
	else if (hero->jumpKey.IsKeyDown())
	{
		hero->ChangeState(&hero->stateJumping);
	}

	if (hero->standingOnObject != nullptr && hero->GetComponent<Collision>()->DoesCollideWith(hero->standingOnObject) == false)
	{
		hero->standingOnObject = nullptr;
		hero->ChangeState(&hero->stateFalling);
	}
}

void Hero::State_Jumping::Enter(GameObject* object)
{
	Hero* hero = static_cast<Hero*>(object);
	hero->velocity.y = hero->Hero::jumpVelocity;
	hero->sprite.PlayAnimation(static_cast<int>(Hero_Anim::Hero_Jump_Anim));
}

void Hero::State_Jumping::Update(GameObject* object, double dt)
{
	Hero* hero = static_cast<Hero*>(object);
	hero->velocity.y -= Engine::GetGameStateManager().GetComponent<Gravity>()->GetValue() * dt;
	hero->UpdateXVelocity(dt);
}

void Hero::State_Jumping::TestForExit(GameObject* object)
{
	Hero* hero = static_cast<Hero*>(object);
	if (hero->jumpKey.IsKeyDown() == false)
	{
		hero->velocity.y = 0.0;
	}
	if (hero->velocity.y <= 0.0)
	{
		hero->ChangeState(&hero->stateFalling);
	}
}

void Hero::State_Falling::Enter(GameObject* object)
{
	Hero* hero = static_cast<Hero*>(object);
	hero->sprite.PlayAnimation(static_cast<int>(Hero_Anim::Hero_Fall_Anim));
}

void Hero::State_Falling::Update(GameObject* object, double dt)
{
	Hero* hero = static_cast<Hero*>(object);
	hero->velocity.y -= Engine::GetGameStateManager().GetComponent<Gravity>()->GetValue() * dt;
	hero->UpdateXVelocity(dt);
}

void Hero::State_Falling::TestForExit(GameObject* object)
{
	Hero* hero = static_cast<Hero*>(object);
	if (hero->GetPosition().y <= hero->sprite.GetFrameSize().y * -2.0)
	{
		hero->isDead = true;
	}
}

void Hero::State_Skidding::Enter(GameObject* object)
{
	Hero* hero = static_cast<Hero*>(object);
	hero->sprite.PlayAnimation(static_cast<int>(Hero_Anim::Hero_Skid_Anim));
}

void Hero::State_Skidding::Update(GameObject* object, double dt)
{
	Hero* hero = static_cast<Hero*>(object);
	if (hero->velocity.x > 0) { hero->velocity.x -= (Hero::xDrag + Hero::xAccel) * dt; }
	else if (hero->velocity.x < 0) {
		hero->velocity.x += (Hero::xDrag + Hero::xAccel) * dt;
	}
}

void Hero::State_Skidding::TestForExit(GameObject* object)
{
	Hero* hero = static_cast<Hero*>(object);
	if (hero->moveLeftKey.IsKeyDown() == true) {
		if (hero->velocity.x <= 0) {
			hero->ChangeState(&hero->stateRunning);
		}
	}
	else if (hero->moveRightKey.IsKeyDown() == true) {
		if (hero->velocity.x >= 0) {
			hero->ChangeState(&hero->stateRunning);
		}
	}
	else {
		hero->ChangeState(&hero->stateRunning);
	}
	if (hero->jumpKey.IsKeyDown() == true) {
		hero->ChangeState(&hero->stateJumping);
	}
}

GameObjectType Hero::GetObjectType()
{
	return GameObjectType::Hero;
}

std::string Hero::GetObjectTypeName()
{
	return "Hero";
}

bool Hero::CanCollideWith(GameObjectType)
{
	return true;
}

void Hero::ResolveCollision(GameObject* collideWith)
{
	Rect collideWithRect = collideWith->GetComponent<RectCollision>()->GetWorldCoorRect();
	Rect heroRect = GetComponent<RectCollision>()->GetWorldCoorRect();

	double heroFeetPos = (heroRect.Left() + heroRect.Right()) / 2.0;
	switch (collideWith->GetObjectType())
	{
	case GameObjectType::Ball:
		// stand on obj
		if (heroRect.Top() > collideWithRect.Top()
			&& (heroFeetPos > collideWithRect.Left() && heroFeetPos < collideWithRect.Right()))
		{
			velocity.y = Hero::jumpVelocity;
		}
		else
		{
			if (heroRect.Left() < collideWithRect.Left())
			{
				velocity = Vector2D{ -maxXVelocity / 3.0, jumpVelocity / 10.0 };
			}
			else
			{
				velocity = Vector2D{ maxXVelocity / 3.0, jumpVelocity / 10.0 };
			}
			ChangeState(&stateJumping);
			hurtTimer = hurtTime;
		}
		break;
	case GameObjectType::Floor: [[fallthrough]];
	case GameObjectType::TreeStump:
		// stand on obj
		if (heroRect.Top() > collideWithRect.Top()
			&& (heroFeetPos > collideWithRect.Left() && heroFeetPos < collideWithRect.Right()))
		{
			SetPositionY(collideWithRect.Top());
			
			standingOnObject = collideWith;

			if (velocity.y < 0.0)
			{
				velocity.y = 0;
				if (velocity.x != 0.0)
				{
					ChangeState(&stateRunning);
				}
				else
				{
					ChangeState(&stateIdle);
				}
			}

		}
		else
		{
			if (heroRect.Left() < collideWithRect.Left())
			{
				if (velocity.x > 0.0)
				{
					SetPositionX(collideWith->GetPosition().x - heroRect.Size().x);
					velocity.x = 0.0;
				}
			}
			else
			{
				if (velocity.x < 0.0)
				{
					SetPositionX(collideWith->GetPosition().x + collideWithRect.Size().x + 3);
					velocity.x = 0.0;
				}
			}
		}
		break;

	case GameObjectType::Bunny:

		// stand on obj
		if (heroRect.Top() > collideWithRect.Top()
			&& (heroFeetPos > collideWithRect.Left() && heroFeetPos < collideWithRect.Right()))
		{
			velocity.y = Hero::jumpVelocity / 2.f;
			collideWith->ResolveCollision(this);
		}
		else
		{
			if (heroRect.Left() < collideWithRect.Left())
			{
				velocity = Vector2D{ -maxXVelocity / 3.0, jumpVelocity / 10.0 };
			}
			else
			{
				velocity = Vector2D{ maxXVelocity / 3.0, jumpVelocity / 10.0 };
			}
			ChangeState(&stateJumping);
			hurtTimer = hurtTime;
		}
		break;
	case GameObjectType::Exit:
		collideWith->ResolveCollision(this);

	}
}

bool Hero::IsDead()
{
	return isDead;
}