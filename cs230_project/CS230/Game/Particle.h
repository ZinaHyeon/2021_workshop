/*--------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name: Particle.h
Purpose: Particle header file
Project: CS230
Author: Kevin Wright
Creation date: 5/19/2020
-----------------------------------------------------------------*/

#pragma once
#include <vector>
#include "..\Engine\BasicDataTypes.h"  //Vector2
#include "..\Engine\GameObject.h"
#include "../Engine/Component.h"

class ParticleEmitter : public Component
{
public:
    ParticleEmitter(std::string spriteFile, int totalParticles, double lifetime);
    void Emit(int toEmit, Vector2D emitterPosition, Vector2D emitterVelocity, Vector2D emitVector, double spread);
    void Clear();
private:
    class Particle : public GameObject {
    public:
        Particle(std::string spriteFile);
        void Revive(Vector2D newPosition, Vector2D newVelocity, double lifetime);
        void Update(double dt) override;
        void Draw(TransformMatrix matrix) override;
        bool IsAlive() { return life > 0; }

        GameObjectType GetObjectType() override;
        std::string GetObjectTypeName() { return "Particle"; }
    private:
        double life;
    };

    int numParticles;
    std::vector<Particle*> particles;
    int particleToUse;
    double lifetime;
};

class HitParticles : public ParticleEmitter {
public:
    HitParticles();
};

class MeteorBitsParticles : public ParticleEmitter {
public:
    MeteorBitsParticles();
};
