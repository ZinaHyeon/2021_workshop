/*--------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name: Collision.cpp
Purpose: Collision source file
Project: CS230
Author: Kevin Wright
Creation date: 4/29/2020
-----------------------------------------------------------------*/

#include "doodle/drawing.hpp"
#include "../Engine/Engine.h" // GetLogger
#include <cmath> // std::pow

#include "Collision.h"
#include "TransformMatrix.h"
#include "GameObject.h"


void RectCollision::Draw(TransformMatrix displayMatrix) 
{
    doodle::no_fill();
    doodle::set_outline_width(2);
    doodle::set_outline_color(0xFFFFFFFF);
    doodle::push_settings();
    doodle::apply_matrix(displayMatrix[0][0], displayMatrix[1][0], displayMatrix[0][1], displayMatrix[1][1], displayMatrix[0][2], displayMatrix[1][2]);
    doodle::draw_rectangle(rect.bottomLeft.x, rect.bottomLeft.y, rect.Size().x, rect.Size().y);
    doodle::pop_settings();
}

Rect RectCollision::GetWorldCoorRect() 
{
    return { objectPtr->GetMatrix() * rect.bottomLeft, objectPtr->GetMatrix() * rect.topRight };
}

bool RectCollision::DoesCollideWith(GameObject* objectB)
{
    if (objectB->GetComponent<RectCollision>() == nullptr)
    {
        Engine::GetLogger().LogError("Doing Rect/Rect collision without Rect/Rect collide objects");
        return false;
    }

    Rect rectA = GetWorldCoorRect();
    Rect rectB = objectB->GetComponent<RectCollision>()->GetWorldCoorRect();

    return rectA.Left() <= rectB.Right() && rectA.Right() >= rectB.Left()
        && rectA.Bottom() <= rectB.Top() && rectA.Top() >= rectB.Bottom();
}

bool RectCollision::DoesCollideWith(Vector2D point)
{
    Rect objRect = GetWorldCoorRect();

    return (point.x > objRect.Left() && point.x < objRect.Right() 
        && point.y > objRect.Bottom() && point.y < objRect.Top());
}

void CircleCollision::Draw(TransformMatrix displayMatrix) 
{
    doodle::no_fill();
    doodle::set_outline_width(2);
    doodle::set_outline_color(0xFFFFFFFF);
    doodle::push_settings();
    doodle::apply_matrix(displayMatrix[0][0], displayMatrix[1][0], displayMatrix[0][1], displayMatrix[1][1], displayMatrix[0][2], displayMatrix[1][2]);
    doodle::draw_ellipse(0, 0, radius * 2);
    doodle::pop_settings();
}

double CircleCollision::GetRadius() 
{
    return (ScaleMatrix(objectPtr->GetScale()) * Vector2D { radius, 0 }).x;
}

bool CircleCollision::DoesCollideWith(GameObject* objectB) {
    if (objectB->GetComponent<CircleCollision>() == nullptr) {
        Engine::GetLogger().LogError("Doing Circle/Circle collision without Circle/Circle collide objects");
        return false;
    }

    CircleCollision* objectBCollision = objectB->GetComponent<CircleCollision>();

    return std::pow(objectPtr->GetPosition().x - objectB->GetPosition().x, 2) + std::pow(objectPtr->GetPosition().y - objectB->GetPosition().y, 2) 
        < std::pow(radius + objectBCollision->GetRadius(), 2);
}

bool CircleCollision::DoesCollideWith(Vector2D point)
{
    return std::pow(objectPtr->GetPosition().x - point.x, 2) + std::pow(objectPtr->GetPosition().y - point.y, 2)
        < std::pow(radius, 2);
}