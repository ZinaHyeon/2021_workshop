/*--------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name: Engine.h
Purpose: Header file for Engine Module
Project: CS230
Author: Kevin Wright
Creation date: 3/19/2020
-----------------------------------------------------------------*/
#include <chrono> // chrono
#include "GameStateManager.h" // GameStateManager
#include "Input.h" // Input
#include "Window.h" // Window
#include "Logger.h" // Logger
#include "SpriteFont.h" // SpriteFont
#include "GameObjectManager.h" // GameObjectManager
#include "TextureManager.h" // TextureManager

class Engine {
public:
	static Engine& Instance() { static Engine instance; return instance; }
	static Logger& GetLogger() { return Instance().logger; };
	static Input& GetInput() { return Instance().input; }
	static Window& GetWindow() { return Instance().window; }
	static GameStateManager& GetGameStateManager() { return Instance().gameStateManager; }
	static GameObjectManager& GetGameObjectManager() { return Instance().gameObjectManager;	}
	static TextureManager& GetTextureManager() { return Instance().textureManager; }

	void ShowCollision(bool newShowCollisionValue) { showCollision = newShowCollisionValue; }
	bool ShowCollision() { return showCollision; }

	void Init(std::string windowName);
	void Shutdown();
	void Update();
	bool HasGameEnded();

	void AddSpriteFont(std::string fileName);
	void DrawText(int fontIndex, Vector2DInt pos, std::string text, SpriteFont::Justified justified = SpriteFont::Justified::Left, Color color = 0xFFFFFFFF);
private:
	Engine();
	~Engine();

	Logger logger;
	GameStateManager gameStateManager;
	Input input;
	Window window;
	GameObjectManager gameObjectManager;
	TextureManager textureManager;

	static constexpr double Target_FPS = 30.0;
	std::chrono::system_clock::time_point lastTick;

	static constexpr double fpsPrintingDelay = 5.0;
	unsigned int frameCount;
	double timer;

	std::vector<SpriteFont> fonts;
	bool showCollision;
};