/*--------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name: BasicDataType.h
Purpose: header file for basic data types
Project: CS230
Author: Haewon Shon
Creation date: 4/1/2020
-----------------------------------------------------------------*/
#pragma once

#include <algorithm> // min, max
#include <cmath>

typedef unsigned Color;

struct Vector2DInt;

struct Vector2D
{
	double x;
	double y;

	Vector2D operator+(const Vector2D& vector) 
	{
		return Vector2D{ this->x + vector.x, this->y + vector.y };
	}

	Vector2D operator-(const Vector2D& vector)
	{
		return Vector2D{ this->x - vector.x, this->y - vector.y };
	}

	Vector2D operator*(double scalar)
	{
		return Vector2D{ this->x * scalar, this->y * scalar };
	}

	Vector2D operator/(double scalar)
	{
		return Vector2D{ this->x / scalar, this->y / scalar };
	}

	Vector2D& operator+=(const Vector2D& vector)
	{
		this->x += vector.x;
		this->y += vector.y;
		return *this;
	}

	Vector2D& operator-=(const Vector2D& vector)
	{
		this->x -= vector.x;
		this->y -= vector.y;
		return *this;
	}

	Vector2D operator-()
	{
		Vector2D negative;
		negative.x = -this->x;
		negative.y = -this->y;
		return negative;
	}
	double LengthSquared() const 
	{
		return (x * x + y * y); 
	}
	Vector2D Normalize() const 
	{ 
		double length = std::sqrt(LengthSquared());  
		return { x / length, y / length }; 
	}
};

struct Vector2DInt
{
	int x;
	int y;

	Vector2DInt operator+(const Vector2DInt& vector)
	{
		return Vector2DInt{ this->x + vector.x, this->y + vector.y };
	}

	Vector2DInt operator-(const Vector2DInt& vector)
	{
		return Vector2DInt{ this->x - vector.x, this->y - vector.y };
	}

	Vector2DInt operator*(int scalar)
	{
		return Vector2DInt{ this->x * scalar, this->y * scalar };
	}

	Vector2DInt operator/(int scalar)
	{
		return Vector2DInt{ this->x / scalar, this->y / scalar };
	}

	Vector2DInt& operator+=(const Vector2DInt& vector)
	{
		this->x += vector.x;
		this->y += vector.y;
		return *this;
	}

	Vector2DInt& operator-=(const Vector2DInt& vector)
	{
		this->x -= vector.x;
		this->y -= vector.y;
		return *this;
	}

	Vector2DInt operator-()
	{
		Vector2DInt negative;
		negative.x = -this->x;
		negative.y = -this->y;
		return negative;
	}

	operator Vector2D()
	{
		return Vector2D{ static_cast<double>(this->x), static_cast<double>(this->y) };
	}
};

struct Rect
{
	Vector2D bottomLeft;
	Vector2D topRight; 
	Vector2D Size() { return { Right() - Left(), Top() - Bottom() }; }
	double Left() { return std::min(bottomLeft.x, topRight.x); }	 // if the rect was flipped, then bottomLeft.x > topRight.x
	double Right() { return std::max(bottomLeft.x, topRight.x); }	// if the rect was flipped, then bottomLeft.x > topRight.x
	double Top() { return topRight.y; }
	double Bottom() { return bottomLeft.y; }
};

struct RectInt {
	Vector2DInt bottomLeft;
	Vector2DInt topRight;
	Vector2DInt Size() { return { Right() - Left(), Top() - Bottom() }; }
	int Left() { return std::min(bottomLeft.x, topRight.x); }	 // if the rect was flipped, then bottomLeft.x > topRight.x
	int Right() { return std::max(bottomLeft.x, topRight.x); }	// if the rect was flipped, then bottomLeft.x > topRight.x
	int Top() { return topRight.y; }
	int Bottom() { return bottomLeft.y; }
};

double Cross(Vector2D v1, Vector2D v2);
int Cross(Vector2DInt v1, Vector2DInt v2);