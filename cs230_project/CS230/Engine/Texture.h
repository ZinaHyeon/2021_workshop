/*--------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name: Texture.h
Purpose: Header file for texture
Project: CS230
Author: Haewon Shon
Creation date: 4/1/2020
-----------------------------------------------------------------*/
#pragma once

#include "doodle/image.hpp"	// doodle::image
#include "BasicDataTypes.h"	//Vector2D, Vector2DInt
#include "TransformMatrix.h" // TransformMatrix

class Texture 
{
    friend class TextureManager;
    friend class SpriteFont;
public:
    void Draw(TransformMatrix displayMatrix, Vector2DInt texelPos, Vector2DInt frameSize);
    Vector2DInt GetSize();
    Color GetPixel(Vector2DInt pos);
private:
    Texture(const std::filesystem::path& filePath);
    doodle::Image image;
    Vector2DInt size;
};
