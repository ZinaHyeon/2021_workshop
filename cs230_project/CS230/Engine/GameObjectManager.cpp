/*--------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name: GameObjectManager.cpp
Purpose: GameObjectManager source file
Project: CS230
Author: Haewon Shon
Creation date: 4/1/2020
-----------------------------------------------------------------*/
#include "GameObjectManager.h"
#include "TransformMatrix.h" // TransformMatrix
#include "Engine.h" // Engine::GetLogger

void GameObjectManager::Add(GameObject* obj)
{
	gameObjects.push_back(obj);
}

void GameObjectManager::Unload()
{
	for (GameObject* obj : gameObjects)
	{
		delete obj;
	}
	gameObjects.clear();
	Engine::GetLogger().LogEvent("Clear Game Objects");
}

void GameObjectManager::UpdateAll(double dt)
{
	std::list<GameObject*> destoryList;
	for (GameObject* object : gameObjects)
	{
		object->Update(dt);
		if (object->ShouldDestory() == true)
		{
			destoryList.push_back(object);
		}
	}
	for (GameObject* object : destoryList)
	{
		gameObjects.remove(object);
		delete object;
	}
}

void GameObjectManager::DrawAll(TransformMatrix& cameraMatrix)
{
	for (GameObject* obj : gameObjects)
	{
		obj->Draw(cameraMatrix);
	}
}

void GameObjectManager::TestForCollisions()
{
	if (gameObjects.empty() != true)
	{
		for (GameObject* object1 : gameObjects)
		{
			for (GameObject* object2 : gameObjects)
			{
				if (object1 == object2)
				{
					continue;
				}

				if (object1->CanCollideWith(object2->GetObjectType()) == true)
				{
					if (object1->DoesCollideWith(object2) == true)
					{
						Engine::GetLogger().LogEvent("Collision Detected: " + object1->GetObjectTypeName() + " and " + object2->GetObjectTypeName());
						object1->ResolveCollision(object2);
					}
				}
			}
		}
	}
}
