/*--------------------------------------------------------------
Copyright (C) 2020 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior
written consent of DigiPen Institute of Technology is prohibited.
File Name: Engine.cpp
Purpose: Source file for Engine Module
Project: CS230
Author: Kevin Wright
Creation date: 3/19/2020
-----------------------------------------------------------------*/
#include "Engine.h"
#include <cstdlib> // srand
#include <ctime> // time

Engine::Engine() : gameStateManager(),
#ifdef _DEBUG
							logger(Logger::Severity::Debug, true)
#else 
							logger(Logger::Severity::Event, false)
#endif
{}

Engine::~Engine() {
}

void Engine::Init(std::string windowName)
{
	logger.LogEvent("Engine Init");
	window.Init(windowName);

	lastTick = std::chrono::system_clock::now();

	unsigned int seed = static_cast<unsigned int>(time(NULL));
	srand(seed);
	logger.LogEvent("Seed = " + std::to_string(seed));
}

void Engine::Shutdown()
{
	logger.LogEvent("Engine Shutdown");
	gameStateManager.Shutdown();
}

void Engine::Update()
{
	std::chrono::system_clock::time_point now = std::chrono::system_clock::now();
	double dt = std::chrono::duration<double>(now - lastTick).count();

	if (dt >= 1 / Engine::Target_FPS)
	{
		++frameCount;
		timer += dt;
		lastTick = std::chrono::system_clock::now();
		logger.LogVerbose("Engine Update");
		gameStateManager.Update(dt);
		input.Update();
		window.Update();
		if (timer >= fpsPrintingDelay)
		{
			double aveFrameRate = frameCount / timer;
			logger.LogEvent("FPS:  " + std::to_string(aveFrameRate));
			timer = 0.0;
			frameCount = 0;
		}
	}
}

bool Engine::HasGameEnded()
{
	return gameStateManager.HasGameEnded();
}

void Engine::AddSpriteFont(std::string fileName) 
{
	fonts.push_back(SpriteFont(fileName));
}

void Engine::DrawText(int fontIndex, Vector2DInt pos, std::string text, SpriteFont::Justified justified, Color color) 
{
	fonts[fontIndex].DrawText(pos, text, justified, color);
}
