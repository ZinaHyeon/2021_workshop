#include "camera.h"
#include "doodle/doodle.hpp"

Camera::Camera(const Vec2& t, const float r, const Vec2& s)
	: translation(t), rotation(r), scaling(s)
{ }

void Camera::PushCamera()
{
	doodle::push_settings();
	doodle::apply_translate(-translation.x, -translation.y);
	doodle::apply_rotate(-rotation);
	doodle::apply_scale(1.0 / scaling.x, 1.0 / scaling.y);
}

void Camera::PopCamera()
{
	doodle::pop_settings();
}