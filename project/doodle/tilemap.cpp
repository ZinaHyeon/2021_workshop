#include "tilemap.h"
#include <fstream>
#include <string>
#include <iostream>
#include <sstream>

/*
* HOW TO USE EDITOR Jan 27th
* - toggle editmode by pressing esc
* - when edit mode is on
* - press mouse without key to add tile
* - press mouse with key to remove tile 
*/

Tilemap::Tilemap() : tile_size(40), tile_height(10) {
    Setup("map.csv"); 
}

Tilemap::~Tilemap()
{ CleanUp(); }

void Tilemap::CleanUp()
{
    if (image)
        delete image;
    image = nullptr;
}

void Tilemap::Setup(std::string mapdataPath) 
{
    std::ifstream mapdata(mapdataPath);

    if (!mapdata.is_open())
    {
        throw std::exception("Failed to open map data");
    }

    std::string input;
    // get rid of first line
    std::getline(mapdata, input);

    // read informatin about width, height, tile size
    std::getline(mapdata, input);
    std::istringstream sstream(input);
    char               c; // for clear
    sstream >> lefttop_x;
    sstream >> c;
    sstream >> lefttop_y;
    sstream >> c;
    sstream >> width;
    sstream >> c;
    sstream >> height;
    sstream >> c;
    sstream >> tile_size;

    double tile_xpos = lefttop_x;
    double tile_ypos = lefttop_y;

    for(int h = 0; h < height; ++h)
    {
        //get new line
        sstream.clear();
        std::getline(mapdata, input);
        sstream.str(input);
        
        for (int w = 0; w < width; ++w)
        {
            int tile;
            sstream >> tile >> c;
            if (tile == 1)
            {
                AddTile(tile_xpos, tile_ypos);
            }
            tile_xpos += tile_size;
        }
        tile_xpos = lefttop_x;
        tile_ypos -= tile_size;
    }

    mapdata.close();
}

void Tilemap::SetupImage(std::string imagePath) 
{
    image = new doodle::Image("assets/tile.png");
}

void Tilemap::ToggleEditMode()
{ 
    isEditingMode = !isEditingMode;

    if (isEditingMode == false)
    {
        Save();
    }
}

void Tilemap::Draw() 
{
    /* temp editing */

    if (isEditingMode)
    {
        doodle::set_font_size(20.0);
        doodle::set_fill_color(0.0);
        doodle::draw_text("in editing mode", -doodle::Width / 2, -doodle::Height / 2);

        if (doodle::MouseIsPressed)
        {
            std::cout << doodle::get_mouse_x() << ", " << doodle::get_mouse_y() << std::endl;
            if (doodle::KeyIsPressed)
            {
                RemoveTile(doodle::get_mouse_x(), doodle::get_mouse_y());
            }
            else
            {
                AddTile(doodle::get_mouse_x(), doodle::get_mouse_y());
            }
        }
    }

    doodle::set_image_mode(doodle::RectMode::Center);
    for (auto& tile : tilemap)
    {
        //doodle::draw_rectangle(tile.second.xpos, tile.second.ypos, tile_size);
        doodle::draw_image(*image, tile.second.xpos, tile.second.ypos, tile_size, tile_height);
    }
}

void Tilemap::AddTile(double xPos, double yPos)
{ 
    int  tilenum = PosToTilenum({xPos, yPos});

    if (tilemap.find(tilenum) != tilemap.end())
    {
        return;
    }
    Tile tile{TilenumToPos(tilenum).x, TilenumToPos(tilenum).y, Tile::TileType::Normal}; 
    tilemap.insert({tilenum, tile});
}

void Tilemap::RemoveTile(double xPos, double yPos)
{
    int tilenum = PosToTilenum({xPos, yPos});
    if (tilemap.find(tilenum) != tilemap.end())
    {
        tilemap.erase(tilenum);
    }
}

/*
*  return the position of the tile with given tile number.
*/
Vec2 Tilemap::TilenumToPos(int tilenum)
{
    int y = (tilenum >> 16) - tilenum_offset;
    int x = (tilenum & 0xffff) - tilenum_offset;

    Vec2 pos;
    pos.x = x * tile_size;
    pos.y = y * tile_size;
    return pos;
}

/*
* return the tile number with given position.
*/ 
int Tilemap::PosToTilenum(const Vec2& pos)
{
    int xpos = (pos.x >= 0.0) ? (static_cast<int>(pos.x) + tile_size / 2) / tile_size
                              : (static_cast<int>(pos.x) - tile_size / 2) / tile_size;

    int ypos = (pos.y >= 0.0) ? (static_cast<int>(pos.y) + tile_size / 2) / tile_size
                              : (static_cast<int>(pos.y) - tile_size / 2) / tile_size;
    
    return ((ypos + tilenum_offset) << 16) | (xpos + tilenum_offset);
}

void Tilemap::Load() 
{
    tilemap.clear();
    Setup("map.csv"); 
}

void Tilemap::Save() 
{ 
    std::ofstream mapdata("map.csv");
    mapdata.clear();

    double right  = -tilenum_offset * tile_size;
    double bottom = tilenum_offset * tile_size;
    lefttop_x     = -right;
    lefttop_y     = -bottom;

    // compute new data
    for (auto& tile : tilemap)
    {
        if (tile.second.xpos < lefttop_x)
        {
            lefttop_x = tile.second.xpos;
        }
        if (tile.second.xpos > right)
        {
            right = tile.second.xpos;
        }
        if (tile.second.ypos > lefttop_y)
        {
            lefttop_y = tile.second.ypos;
        }
        if (tile.second.ypos < bottom)
        {
            bottom = tile.second.ypos;
        }
    }

    width  = static_cast<int>(right - lefttop_x) / tile_size + 1;
    height = static_cast<int>(lefttop_y - bottom) / tile_size + 1; 

    // file format
    mapdata << "lefttop_x, lefttop_y, width, height, tilesize," << std::endl;
    mapdata << lefttop_x << "," << lefttop_y << "," << width << "," << height << "," << tile_size << "," << std::endl;

    // save current data

    int tilenum = PosToTilenum({lefttop_x, lefttop_y});

    for (int h = 0; h < height; ++h)
    {
        for (int w = 0; w < width; ++w)
        {
            // tile exist
            if (tilemap.find(tilenum) != tilemap.end())
            {
                mapdata << static_cast<int>(tilemap[tilenum].tiletype) << ",";
            }
            // tile not exist
            else
            {
                mapdata << 0 << ",";
            }
            tilenum += 1;
        }
        mapdata << std::endl;
        tilenum -= width;
        tilenum -= 0x10000;
    }

    mapdata.close();
}

Tilemap::Tile Tilemap::GetTileOnPosition(const Vec2& pos)
{
    int tilenum = PosToTilenum(pos);
    if (tilemap.find(PosToTilenum(pos)) != tilemap.end())
    {
        if (abs(pos.y - tilemap[tilenum].ypos) > tile_height / 2.0)
        {
            return Tile{0.0, 0.0, Tilemap::Tile::TileType::INVALID};
        }
        return tilemap[tilenum];
    }
    else
    {
        return Tile{0.0, 0.0, Tilemap::Tile::TileType::INVALID};
    }
}