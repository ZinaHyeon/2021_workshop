#pragma once

/*
*  Camera.h
* 
*  - Header for Camera abstract class.
*  - This will store camera TRS information, and will be used to draw objects.
*  - To use, define inherited class, and register it.
*/

#include "vec2.h"

class Camera
{
public:
	Camera(const Vec2& t, const float r, const Vec2& s);
	virtual ~Camera() {}
	virtual void Update(double dt) = 0;
	void PushCamera();
	void PopCamera();

	inline Vec2 GetTranslation() { return translation; }
	inline double GetRotation() { return rotation; }
	inline Vec2 GetScaling () { return scaling; }

	inline void SetTranslation(const Vec2& t) { translation = t; }
	inline void SetRotation(const double r) { rotation = r; }
	inline void SetScaling(const Vec2& s) { scaling = s; }
protected:
	Vec2 translation;
	double rotation;
	Vec2 scaling;
};