#pragma once

struct Vec2
{
    double x = 0.f, y = 0.f;
    double operator[](int index) const;
    double& operator[](int index);
};

Vec2 operator-(const Vec2&);
Vec2 operator+(const Vec2&, const Vec2&);
void operator+=(Vec2&, const Vec2&);
Vec2 operator-(const Vec2&, const Vec2&);
void operator-=(Vec2&, const Vec2&);
Vec2 operator*(const double&, const Vec2&);
Vec2 operator*(const Vec2&, const double&);
bool operator==(const Vec2&, const Vec2&);
bool operator!=(const Vec2&, const Vec2&);

double dot(const Vec2&, const Vec2&);
double length(const Vec2&);
Vec2 normalize(const Vec2&);
double min(double, double);
Vec2 min(const Vec2&, const Vec2&);
Vec2 project(const Vec2&, const Vec2&);
Vec2 abs(const Vec2&);