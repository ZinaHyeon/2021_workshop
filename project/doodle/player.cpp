#include "player.h"
//#include "globals.h"          // tilemap
#include <doodle/drawing.hpp> // draw_rectangle

using namespace doodle;

namespace Control
{
    static bool left  = false;
    static bool right = false;
    static bool up    = false;
    static bool down          = false;

    void SetPressedKey(doodle::KeyboardButtons button)
    {
        if (button == Control::LEFT)
            left = true;
        if (button == Control::RIGHT)
            right = true;
        if (button == Control::UP)
            up = true;
        if (button == Control::DOWN)
            down = true;
    }

    void SetRelasedKey(doodle::KeyboardButtons button)
    {
        if (button == Control::LEFT)
            left = false;
        if (button == Control::RIGHT)
            right = false;
        if (button == Control::UP)
            up = false;
        if (button == Control::DOWN)
            down = false;
    }
}

namespace Platform
{
    bool OnGround([[maybe_unused]]const double x_pos, [[maybe_unused]] double bottom, [[maybe_unused]] double& near_ground)
    {
        bool on_ground = false;
        // get tile on the player's position

        // TODO : Connect it with level's tilemap

        //Tilemap::Tile tile = tilemap.GetTileOnPosition({x_pos, bottom});
        //if (tile.tiletype == Tilemap::Tile::TileType::Normal)
        //{
        //    on_ground   = true;
        //    near_ground = tile.ypos + tilemap.GetTileHeight() / 2;
        //}

        return on_ground;
    }
}

Player::Player(const doodle::Color& c, const Vec2& p, const Vec2& s) : Object(c, p, s)
{
    type              = Type::PLAYER;
    double near_ground = 0;
    grounded          = Platform::OnGround(position.x, position.y - (size.y / 2.f), near_ground);
}

Player::Player(const std::string& image_path, const Vec2& p, const Vec2& s, int frame_count)
    : Object(image_path, p, s, frame_count)
{
    type               = Type::PLAYER;
    double near_ground = 0;
    grounded          = Platform::OnGround(position.x, position.y - (size.y / 2.f), near_ground);
}

Player::~Player() {}

void Player::update([[maybe_unused]] double dt)
{
    switch (game_type)
    {
        case GameType::PLATFORMER:
        {
            static const Vec2 up = Vec2{0.0, 1.0};
            Vec2              horizontal_velocity;
            if (Control::left)
                horizontal_velocity.x -= 1.0;
            if (Control::right)
                horizontal_velocity.x += 1.0;

            if (Control::up) // jump
            {
                if (grounded)
                {
                    velocity   = Vec2{velocity.x, max_height};
                    grounded   = false;
                    is_jumping = true;
                }
            }
            else if (is_jumping) // jump cancel
            {
                is_jumping = false;
                velocity.y *= 0.5;
            }

            Vec2 vertical_velocity = project(velocity, up);

            if (!grounded || is_jumping_down) // above platform
                vertical_velocity -= gravity * dt * up;
            else
                vertical_velocity = Vec2{};

            velocity = vertical_velocity + horizontal_velocity;
            position += dt * acceleration * Vec2{velocity.x * move_speed, velocity.y * jump_speed};

            if (dot(velocity, up) < 0.0) // downward
                is_jumping = false;

            if (!is_jumping) // if not moving upward
            {
                double       near_ground = 0.0;
                const double half_height = size.y * 0.5;
                grounded                 = Platform::OnGround(position.x, position.y - half_height, near_ground);
                if (grounded)
                {
                    if (Control::down) // move down
                    {
                        is_jumping_down = true;
                        grounded      = false;
                    }
                    else
                    {
                        if (!is_jumping_down)
                        {
                            position.y = near_ground + half_height;
                        }
                        
                    }
                }
                else
                {
                    is_jumping_down = false;
                }
            }
        }
        break;
        case GameType::TOPDOWN:
        {
            velocity = Vec2{};
            if (Control::left)
                velocity.x -= 1.0;
            if (Control::right)
                velocity.x += 1.0;
            if (Control::up)
                velocity.y += 1.0;
            if (Control::down)
                velocity.y -= 1.0;

            position += move_speed * normalize(velocity);
        }
    }

    // flip image when moving direction is changed
    if (velocity.x != 0.0)
    {
        if (velocity.x < 0.0)
            sprite.should_flip = true;
        else
            sprite.should_flip = false;
    }
}