#pragma once

#include <map>
#include <string>
#include "Level.h"

class LevelManager
{
public:
	void Init();
	void Update(double dt);
	void Draw(double dt);
	void Clear();

	void RegisterLevel(Level* level);
	void SetNextLevel(std::string levelName);
	inline std::string GetCurrentLevelName() { return currentLevel->GetName(); }
private:
	std::map<std::string, Level*> levels;
	Level* currentLevel = nullptr;
	Level* nextLevel = nullptr;
};