#include "sprite.h"
#include <doodle/drawing.hpp>

using namespace doodle;

void Animation::init(int image_width, int image_height, int fc)
{
    frame_count = fc;
    width       = image_width / frame_count;
    height      = image_height;
    texel.clear();
    for (int i = 0; i < frame_count; i++)
    {
        texel.push_back(width * i);
    }
}

void Animation::update(double dt) const
{
    if (state == AnimationState::Play)
    {
        time += dt;

        if (time > target_time)
        {
            current_frame = (current_frame + 1) % frame_count;
            time          = 0.0;
        }
    }
}

void Animation::speedup(double ratio) { target_time *= ratio; }

void Animation::Pause(int frame)
{
    state = AnimationState::Pause;
    if (frame != -1)
    {
        const int& size = static_cast<int>(texel.size()) - 1;
        if (frame < 0 || size < frame)
            throw std::runtime_error("Animation: Frame number should be between -1 and " + std::to_string(size) +
                                     "!\n");
        current_frame = frame;
    }
}

void Animation::Play() { state = AnimationState::Play; }

void Sprite::init(const std::string& image_path, int frame_count)
{
    image = new Image(image_path);
    if (frame_count == 0)
        type = SpriteType::Image;
    else
    {
        type = SpriteType::Animation;
        animation.init(image->GetWidth(), image->GetHeight(), frame_count);
    }
}

void Sprite::init(const Color& c) { color = c; }

void Sprite::draw([[maybe_unused]] double dt, const Vec2& position, const Vec2& size) const
{
    const double& width = (should_flip ? -size.x : size.x);

    push_settings();
    set_outline_color(0, 0);
    switch (type)
    {
        case SpriteType::Color:
            set_fill_color(color);
            draw_rectangle(position.x, position.y, width, size.y);
            break;
        case SpriteType::Image: draw_image(*image, position.x, position.y, width, size.y); break;
        case SpriteType::Animation:
        {
            animation.update(dt);
            draw_image(*image, position.x, position.y, width, size.y, animation.texel[animation.current_frame], 0,
                       animation.width, animation.height);
        }
        break;
    }

    pop_settings();
}

Animation* Sprite::SetAnimation() { return &animation; }

void Sprite::clean()
{
    if (image)
    {
        delete image;
        image = nullptr;
    }
}
