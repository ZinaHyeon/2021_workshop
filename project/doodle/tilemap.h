#pragma once

#include "vec2.h"
#include <doodle/doodle.hpp>
#include <unordered_map>

class Tilemap
{
public:
    struct Tile
    {
        enum TileType
        {
            INVALID, Normal = 1,
        };
        double   xpos, ypos;
        TileType tiletype;
    };

    Tilemap();
    ~Tilemap();
    void ToggleEditMode();
    void Draw();
    void SetupImage(std::string imagePath);
    void Load();
    void Save();
    void CleanUp();
    Tile GetTileOnPosition(const Vec2& pos);
    inline int GetTileSize() const { return tile_size; };
    inline double GetTileHeight() const { return tile_height; }

private:
    void Setup(std::string mapdataPath);
    void AddTile(double xPos, double yPos);
    void RemoveTile(double xPos, double yPos);
    Vec2 TilenumToPos(int tilenum);
    int  PosToTilenum(const Vec2& pos);

    double                        lefttop_x;
    double                        lefttop_y;
    int                           width;
    int                           height;
    int                           tile_size;
    double                        tile_height;
    std::unordered_map<int, Tile> tilemap;
    doodle::Image*                image;

    bool isEditingMode = false;

    const int tilenum_offset = 0x1000;
};