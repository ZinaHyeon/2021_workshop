#include "LevelManager.h"
#include <iostream>

void LevelManager::Init()
{
	if (currentLevel == nullptr)
	{
		throw std::exception("Current level is nullptr.\n Use \"SetCurrentLevel\" function to set current Level.");
	}

	currentLevel->Init();
}

void LevelManager::Update(double dt)
{
	if (nextLevel != nullptr)
	{
		currentLevel->Clear();
		currentLevel = nextLevel;
		nextLevel = nullptr;
		currentLevel->Init();
	}
	currentLevel->Update(dt);
}

void LevelManager::Draw(double dt)
{
	currentLevel->Draw(dt);
}

void LevelManager::Clear()
{
	currentLevel->Clear();
}

void LevelManager::RegisterLevel(Level* level)
{
	levels.insert({level->GetName(), level});

	// if current level is not set, set given level as current one
	if (currentLevel == nullptr)
	{
		currentLevel = level;
	}
}

void LevelManager::SetNextLevel(std::string levelName)
{
	if (levels.find(levelName) != levels.end())
	{
		nextLevel = levels[levelName];
	}
	else
	{
		throw std::exception("The level is not found.");
	}
}
