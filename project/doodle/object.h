#pragma once
#include "vec2.h" // vec2
#include "sprite.h"
#include <doodle/color.hpp> // color
#include <doodle/image.hpp> // Image

enum class Type
{
    NONE,
    PLAYER
};

struct Object
{
public:
    Object(const doodle::Color& c, const Vec2& p, const Vec2& s)
    {
        position = p;
        size = s;
        sprite.init(c);
    }
    Object(const std::string& image_path, const Vec2& p, const Vec2& s, int frame_count = 0)
    {
        position = p;
        size = s;
        sprite.init(image_path, frame_count);
    }
    virtual ~Object() = default;
    void         draw(double dt) const { sprite.draw(dt, position, size); }
    virtual void update([[maybe_unused]] double dt) {}
    void         clean() { sprite.clean(); }
    Animation* animation() { return sprite.SetAnimation(); }
    Vec2         Position() const { return position; }
    Vec2         Size() const { return size; }
protected:
    Vec2   position, size;
    Type   type = Type::NONE;
    Sprite sprite;
};