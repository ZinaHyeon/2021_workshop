#include <doodle/doodle.hpp>
#include <iostream>
#include "globals.h"
#include "player.h"

using namespace doodle;

int main(void)
try
{
    create_window(800, 600);
    set_rectangle_mode(RectMode::Center);

    player = new Player("assets/character.png", Vec2{0, 0}, Vec2{200, 250}, 8);
    player->game_type = GameType::TOPDOWN;

    tilemap.SetupImage("assets/tile.png");

    while (!is_window_closed())
    {
        update_window();
        clear_background(250);
        tilemap.Draw();

        player->update(DeltaTime);
        player->draw(DeltaTime);
    }
    player->clean();

    tilemap.CleanUp();
    return 0;
}
catch (const std::exception& e)
{
    std::cout << e.what() << std::endl;
    return -1;
}


void on_key_pressed(doodle::KeyboardButtons button)
{
    Control::SetPressedKey(button);
    if (button == doodle::KeyboardButtons::Space)
    {
        tilemap.ToggleEditMode();
    }

    if (button == doodle::KeyboardButtons::Escape)
    {
        close_window();
    }

    if (button == doodle::KeyboardButtons::F1)
    {
        if (player->game_type == GameType::TOPDOWN)
        {
           player->game_type = GameType::PLATFORMER;
        }
        else
        {
            player->game_type = GameType::TOPDOWN;
        }
    }

    static bool toggle_animation = true;
    if (button == KeyboardButtons::_1)
    {
        toggle_animation = !toggle_animation;
        if (toggle_animation)
            player->animation()->Play();
        else
            player->animation()->Pause();
    }

    if (button == KeyboardButtons::_2)
        player->animation()->speedup(0.5);
    if (button == KeyboardButtons::_3)
        player->animation()->speedup(2.0);
}

void on_key_released(doodle::KeyboardButtons button) { Control::SetRelasedKey(button); }