#pragma once
#include "object.h"
#include <doodle/input.hpp> // KeyboardButtons

namespace Control
{
    static doodle::KeyboardButtons LEFT  = doodle::KeyboardButtons::A;
    static doodle::KeyboardButtons RIGHT = doodle::KeyboardButtons::D;
    static doodle::KeyboardButtons UP    = doodle::KeyboardButtons::W;
    static doodle::KeyboardButtons DOWN  = doodle::KeyboardButtons::S;

    void SetPressedKey(doodle::KeyboardButtons button);
    void SetRelasedKey(doodle::KeyboardButtons button);
}

enum class GameType
{
    PLATFORMER,
    TOPDOWN
};

struct Player : public Object
{
    Player(const doodle::Color& c, const Vec2& p, const Vec2& s);
    Player(const std::string& image_path, const Vec2& p, const Vec2& s, int frame_count = 0);
    ~Player() override;
    void update(double dt) override;

    GameType game_type = GameType::PLATFORMER;
private:
    bool grounded   = true;
    bool is_jumping = false;
    bool is_jumping_down = false;

    Vec2 velocity = Vec2{0.0, 0.0};

    const double acceleration = 20.0;
    const double move_speed = 8.0;
    const double jump_speed = 2.0;
    const double gravity    = 9.8;
    const double max_height = 8.0;
};