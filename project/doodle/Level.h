#pragma once

#include <list>
#include <string>
#include "tilemap.h"

/*
*  Level.h
*
*  - Header for Level abstract class.
*  - To use, define inherited class.
*/

class Camera;
struct Object;

class Level
{
public:
	virtual void Init() = 0;
	virtual void Update(double dt) = 0;
	virtual void Draw(double dt) = 0;
	virtual void Clear() = 0;
	virtual std::string GetName() = 0;
protected:
	std::list<Object*> objects;
	Camera* camera = nullptr;
	Tilemap tilemap;
};