#pragma once
#include "vec2.h"
#include <doodle/color.hpp>
#include <doodle/image.hpp>

enum class SpriteType
{
    Color,
    Image,
    Animation
};

struct Sprite;
struct Animation
{
    friend Sprite;
    void speedup(double ratio);
    void Pause(int frame = -1);
    void Play();

private:
    void init(int image_width, int image_height, int frame_count);
    void update(double dt) const;

    enum class AnimationState
    {
        Play,
        Pause
    };

    std::vector<int> texel;
    int              width = 0, height = 0;
    int              frame_count   = 0;
    mutable int      current_frame = 0;
    mutable double   time          = 0.0;
    double           target_time   = 0.1;
    AnimationState   state         = AnimationState::Play;
};

struct Sprite
{
    void init(const std::string& image_path, int fc = 0);
    void init(const doodle::Color&);

    void       draw(double dt, const Vec2& position, const Vec2& size) const;
    Animation* SetAnimation();

    void clean();

    bool should_flip = false;

private:
    SpriteType     type  = SpriteType::Image;
    doodle::Image* image = nullptr;
    doodle::Color  color = doodle::Color(255);
    Animation      animation;
};
