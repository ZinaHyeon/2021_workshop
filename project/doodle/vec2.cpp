#include "vec2.h"
#include <cmath>

double Vec2::operator[](int index) const { return *(&x + index); }

double& Vec2::operator[](int index) { return *(&x + index); }

Vec2 operator-(const Vec2& v) { return Vec2{-v.x, -v.y}; }

Vec2 operator+(const Vec2& v1, const Vec2& v2) { return Vec2{v1.x + v2.x, v1.y + v2.y}; }

void operator+=(Vec2& v1, const Vec2& v2)
{
    v1.x += v2.x;
    v1.y += v2.y;
}

Vec2 operator-(const Vec2& v1, const Vec2& v2) { return Vec2{v1.x - v2.x, v1.y - v2.y}; }

void operator-=(Vec2& v1, const Vec2& v2)
{
    v1.x -= v2.x;
    v1.y -= v2.y;
}

Vec2 operator*(const double& f, const Vec2& v) { return Vec2{f * v.x, f * v.y}; }

Vec2 operator*(const Vec2& v, const double& f) { return f * v; }

bool operator==(const Vec2& v1, const Vec2& v2) { return (v1.x == v2.x) && (v1.y == v2.y); }

bool operator!=(const Vec2& v1, const Vec2& v2) { return (v1.x != v2.x) || (v1.y != v2.y); }

double dot(const Vec2& v1, const Vec2& v2) 
{
    return (v1.x * v2.x) + (v1.y * v2.y);
}

double length(const Vec2& v) 
{
    return std::sqrt(dot(v, v));
}

Vec2 normalize(const Vec2& v)
{
    if (v.x == 0.0 && v.y == 0.0)
        return v;

    const double d = 1.0 / std::sqrt((v.x * v.x) + (v.y * v.y));
    return d * v;
}

double min(double f1, double f2) 
{
    return f1 < f2 ? f1 : f2;
}

Vec2 min(const Vec2& v1, const Vec2& v2)
{
    return Vec2{min(v1.x, v2.x), min(v1.y, v2.y)};
}

Vec2 project(const Vec2& v, const Vec2& u)
{
    const double mag   = length(u);
    const double scale = dot(u, v) / (mag * mag);
    return scale * u;
}

Vec2 abs(const Vec2& v)
{
    return Vec2{ abs(v.x), abs(v.y) };
}
