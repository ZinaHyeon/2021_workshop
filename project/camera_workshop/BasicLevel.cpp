#include "BasicLevel.h"
#include "BasicCamera.h"
#include "player.h"

void BasicLevel::Init()
{
	player = new Player("assets/character.png", Vec2{ 0, 0 }, Vec2{ 60, 75 }, 8);
	player->game_type = GameType::TOPDOWN;
	tilemap.SetupImage("assets/tile.png");
	camera = new BasicCamera({ 0.0, 0.0 }, 0.0, { 1.0, 1.0 });
	dynamic_cast<BasicCamera*>(camera)->SetTarget(player);
}

BasicLevel::~BasicLevel() {}

void BasicLevel::Update(double dt)
{
	player->update(dt);
	camera->Update(dt);
}

void BasicLevel::Draw(double dt)
{
	camera->PushCamera();
	tilemap.Draw();
	player->draw(dt);

	camera->PopCamera();
}

void BasicLevel::Clear()
{
	if (player) delete player;
	if (camera) delete camera;
}
