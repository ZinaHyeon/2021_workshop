#pragma once

#include "Level.h"

struct Player;

class BasicLevel : public Level
{
public:
	virtual ~BasicLevel();
	void Init() override;
	void Update(double dt) override;
	void Draw(double dt) override;
	void Clear() override;
	inline std::string GetName() { return name; }
private:
	Player* player = nullptr;
	std::string name = "BasicLevel";
};