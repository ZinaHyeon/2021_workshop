#pragma once

#include "camera.h"
#include "object.h"

class BasicCamera : public Camera
{
public:
	BasicCamera(const Vec2& t, const float r, const Vec2& s);
	void SetTarget(Object*);
	void Update(double dt) override;
private:
	void DrawRange();
	Vec2 half_range;
	Object* target = nullptr;
};
