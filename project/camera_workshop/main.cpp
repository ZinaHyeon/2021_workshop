#include <doodle/doodle.hpp>
#include <iostream>
#include "player.h"
#include "LevelManager.h"
#include "BasicLevel.h"

using namespace doodle;

int main(void)
try
{
    create_window(800, 600);
    set_rectangle_mode(RectMode::Center);

    //1. RegisterLevel - should be at least one level
    LevelManager lManager;
    lManager.RegisterLevel(new BasicLevel());

    // 2. init
    lManager.Init();

    while (!is_window_closed())
    {
        clear_background(250);
        lManager.Update(DeltaTime);
        lManager.Draw(DeltaTime);
        update_window();
    }
    return 0;
}
catch (const std::exception& e)
{
    std::cout << e.what() << std::endl;
    return -1;
}


void on_key_pressed(doodle::KeyboardButtons button)
{
    Control::SetPressedKey(button); // enable to control the player

    if (button == doodle::KeyboardButtons::Escape)
        doodle::close_window();

    /*if (button == doodle::KeyboardButtons::F1)
    {
        if (player->game_type == GameType::TOPDOWN)
        {
           player->game_type = GameType::PLATFORMER;
        }
        else
        {
            player->game_type = GameType::TOPDOWN;
        }
    }

    static bool toggle_animation = true;
    if (button == KeyboardButtons::_1)
    {
        toggle_animation = !toggle_animation;
        if (toggle_animation)
            player->animation()->Play();
        else
            player->animation()->Pause();
    }

    if (button == KeyboardButtons::_2)
        player->animation()->speedup(0.5);
    if (button == KeyboardButtons::_3)
        player->animation()->speedup(2.0);*/
}

void on_key_released(doodle::KeyboardButtons button) { Control::SetRelasedKey(button); }