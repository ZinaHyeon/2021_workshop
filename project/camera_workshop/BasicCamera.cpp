#include "BasicCamera.h"
#include <doodle/drawing.hpp>

BasicCamera::BasicCamera(const Vec2& t, const float r, const Vec2& s)
	: Camera(t, r, s)
{
	half_range = Vec2{ 100,100 };
}

void BasicCamera::SetTarget(Object* object)
{
	target = object;
}

void BasicCamera::Update([[maybe_unused]] double dt)
{
	if (target)
	{
		const Vec2 pos = target->Position() - translation;
		const Vec2 size = target->Size() * 0.5;

		if (pos.x + size.x > half_range.x)
			translation.x = target->Position().x + size.x - half_range.x;
		else if (pos.x - size.x < -half_range.x)
			translation.x = target->Position().x - size.x + half_range.x;
		
		if (pos.y + size.y> half_range.y)
			translation.y = target->Position().y + size.y - half_range.y;
		else if (pos.y - size.y < -half_range.y)
			translation.y = target->Position().y - size.y + half_range.y;
	}

	DrawRange();
}

void BasicCamera::DrawRange()
{
	using namespace doodle;
	push_settings();
	set_outline_color(255, 0, 0);
	set_fill_color(0, 0);
	draw_rectangle(0, 0, half_range.x * 2.0, half_range.y * 2.0);
	pop_settings();
}
